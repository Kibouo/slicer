CXX = g++

FILE_NAME = slicer
DEBUG_FILE_NAME= slicer_debug
BUILD_DIR = ./build

WARNING_SETTINGS = -Wall -Wextra -Werror

COMPILER_FLAGS += -std=c++17 -DGL_GLEXT_PROTOTYPES
COMPILER_FLAGS += $(shell pkg-config --cflags gtk+-3.0 assimp)
COMPILER_FLAGS += -I./lib/fmt/include -I./src -I./lib/clipper

LIBS += $(shell pkg-config --libs gtk+-3.0 assimp)
LIBS += -lm

detected_OS := $(shell uname)
ifeq ($(detected_OS),Darwin)
   LIBS += -framework OpenGL
endif
ifeq ($(detected_OS),Linux)
	COMPILER_FLAGS += $(shell pkg-config --cflags gl)
    LIBS += $(shell pkg-config --libs gl)
endif

DEBUG_FLAGS = -g
RELEASE_FLAGS = -O3

# `-w clipper` disables all warnings in Clipper. 
# This is done as we treat every warning as an error for clean code reasons.
SOURCE_FILES += $(shell find src/ -name "*.cpp")
SOURCE_FILES += ./lib/fmt/src/format.cc -w ./lib/clipper/clipper.cpp

release: COMPILER_FLAGS += $(RELEASE_FLAGS)
release: all

debug: COMPILER_FLAGS += $(DEBUG_FLAGS)
debug: FILE_NAME = $(DEBUG_FILE_NAME)
debug: all

all: 
	mkdir -p $(BUILD_DIR)
	$(CXX) $(WARNING_SETTINGS) $(COMPILER_FLAGS) $(LIBS) $(SOURCE_FILES) -o $(BUILD_DIR)/$(FILE_NAME) 

clean: 
	rm -rf $(BUILD_DIR)