
# COFAB Slicer - Group 
Authors: Mihály Csonka & Ward Habraken
## Installation & Getting started

### prerequisites

1. install OpenGL 4.6
2. install GTK 3.24 from https://www.gtk.org/
3. install GLM 0.9
4. install Assimp 5.0
5. download [Clipper 6.4](https://sourceforge.net/projects/polyclipping/files/clipper_ver6.4.2.zip/download) and place it in `lib/clipper`
6. Add fmt submodule to `lib/` via git [fmt](https://github.com/fmtlib/fmt) with the following commands: `git submodule init` and `git pull --recurse-submodules`


### Build the application

A `make` file is provided to compile the application on Linux and Mac OS, with the correct flags. Simply run: 

```[zsh]
make release
```

This will create an executable for your OS in the `build` directory. To run the application execute the following command:

```[zsh]
./build/slicer
```
You will be greeted by the following screens, but with support disabled: 

<img src="resources/docs/2d_gui.png" alt="drawing" width="400"/>
<img src="resources/docs/3d_gui.png" alt="drawing" width="400"/>


## Controls
| Key | Fucntion                                                                                                                                  |
| --- | ----------------------------------------------------------------------------------------------------------------------------------------- |
| e   | Raise the slicing plane by 1 slice height                                                                                                 |
| q   | Lower the slicing plane by 1 slice height                                                                                                 |
| g   | Generate gcode for the currently loaded model, using the current settings. The generated gcode file is stored next to the launched binary |




