#version 330 core

in vec4  f_color;
out vec4 color;

void main() { 
	color = mix(f_color, vec4(0.2, 0.2, 0.2, 1.0), 0.5); 
	//color = f_color;
}