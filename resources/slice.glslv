#version 330 core

layout(location = 0) in vec2 vertex;

uniform mat4 view_matrix;
uniform mat4 projection_matrix;
uniform mat4 model_matrix;
uniform vec3 color;

out vec3 f_color;

void main()
{
    mat4 mvp      = projection_matrix * view_matrix * model_matrix;
    vec4 position = mvp * vec4(vertex[0], vertex[1], 1.0, 1.0);
    // set depth/distance fixed as it doesn't matter for an ortho camera.
    // Setting it ourselves because max render distance is -1, but mult with mvp
    // resulted in some data being further away.
    position.z  = -0.5;
    gl_Position = position;

    f_color = color;
}
