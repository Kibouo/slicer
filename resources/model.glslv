#version 330 core

layout(location = 0) in vec3 vertex;

uniform mat4 view_matrix;
uniform mat4 projection_matrix;
uniform mat4 model_matrix;
uniform vec4 color;

out vec4 f_color;

void main()
{
    mat4 mvp    = projection_matrix * view_matrix * model_matrix;
    gl_Position = mvp * vec4(vertex, 1.0);

    f_color = color;
}
