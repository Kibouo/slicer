#include "include/gui/event_handlers.hpp"
#include "include/engine/engine.hpp"
#include "include/engine/gcode_generator.hpp"
#include "include/gui/settings_dialog.hpp"
#include <iostream>

#ifdef __APPLE__
//#include <OpenGL/gl.h>
#define GL_SILENCE_DEPRECATION
#include <OpenGL/gl3.h>
#else
#include <GL/gl.h>
#endif

gboolean on_realize(GtkGLArea* glarea, std::optional<Engine*>* engine)
{
    gtk_gl_area_make_current(glarea);
    gtk_gl_area_set_has_depth_buffer(glarea, true);

    // The frame clock triggers an event each time OpenGL wants to update
    // the frame.
    GdkGLContext*  glcontext   = gtk_gl_area_get_context(glarea);
    GdkWindow*     glwindow    = gdk_gl_context_get_window(glcontext);
    GdkFrameClock* frame_clock = gdk_window_get_frame_clock(glwindow);

    g_signal_connect_swapped(
        frame_clock, "update", G_CALLBACK(gtk_gl_area_queue_render), glarea);
    gdk_frame_clock_begin_updating(frame_clock);

    if (engine->has_value()) { delete engine->value(); }
    *engine = std::optional{ new Engine() };
    return true;
}

gboolean on_main_render([[maybe_unused]] GtkGLArea*    _,
                        [[maybe_unused]] GdkGLContext* __,
                        std::optional<Engine*>*        engine)
{
    glClearColor(0.0f, 0.0f, 0.4f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    engine->value()->draw_model();

    glFlush();
    return true;
}

gboolean on_slice_render([[maybe_unused]] GtkGLArea*    _,
                         [[maybe_unused]] GdkGLContext* __,
                         std::optional<Engine*>*        engine)
{
    glClearColor(0.0f, 0.0f, 0.4f, 0.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    engine->value()->draw_slice();

    glFlush();
    return true;
}

void on_main_resize([[maybe_unused]] GtkGLArea* _,
                    gint                        width,
                    gint                        height,
                    std::optional<Engine*>*     engine)
{
    engine->value()->model_camera->aspect_ratio(width, height);
}

void on_slice_resize([[maybe_unused]] GtkGLArea* _,
                     gint                        width,
                     gint                        height,
                     std::optional<Engine*>*     engine)
{
    engine->value()->slice_camera->aspect_ratio(width, height);
}

void on_load_stl(GtkCheckMenuItem* menu_item)
{
    auto const engine = ((std::optional<Engine*>*)g_object_get_data(
                             G_OBJECT(menu_item), ENGINE_KEY.c_str()))
                            ->value();
    auto const window = GTK_WINDOW(
        g_object_get_data(G_OBJECT(menu_item), WINDOW_KEY.c_str()));

    auto const file_chooser
        = gtk_file_chooser_dialog_new("Load Model",
                                      window,
                                      GTK_FILE_CHOOSER_ACTION_OPEN,
                                      "Cancel",
                                      GTK_RESPONSE_CANCEL,
                                      "Open",
                                      GTK_RESPONSE_ACCEPT,
                                      NULL);

    if (gtk_dialog_run(GTK_DIALOG(file_chooser)) == GTK_RESPONSE_ACCEPT)
    {
        char* filename
            = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(file_chooser));

        engine->load_model(std::filesystem::path(filename));

        // gtk allocated the string so we let it do the clean up
        g_free(filename);
    }

    gtk_widget_destroy(file_chooser);
}

void on_edit_settings(GtkCheckMenuItem* menu_item)
{
    auto const engine = ((std::optional<Engine*>*)g_object_get_data(
                             G_OBJECT(menu_item), ENGINE_KEY.c_str()))
                            ->value();
    auto const window = GTK_WINDOW(
        g_object_get_data(G_OBJECT(menu_item), WINDOW_KEY.c_str()));

    auto const settings_dialog
        = SettingsDialog(window, engine->printer_config());

    auto done = false;
    while (!done)
    {
        if (settings_dialog.run() == GTK_RESPONSE_ACCEPT)
        {
            auto const result = settings_dialog.try_into_printer_config();

            if (result.second)
            {
                auto const error_msg = result.second.value().c_str();
                auto const error_dialog
                    = gtk_message_dialog_new(window,
                                             GTK_DIALOG_MODAL,
                                             GTK_MESSAGE_ERROR,
                                             GTK_BUTTONS_CLOSE,
                                             error_msg);
                gtk_dialog_run(GTK_DIALOG(error_dialog));
                gtk_widget_destroy(error_dialog);
            }
            else
            {
                engine->update_printer_config(result.first.value());
                done = true;
            }
        }
        else
        {
            done = true;
        }
    }
}

gboolean on_key_press([[maybe_unused]] GtkWindow* _,
                      GdkEventKey*                event,
                      std::optional<Engine*>*     engine)
{
    switch (event->keyval)
    {
        case GDK_KEY_e:
        {
            engine->value()->slicing_plane_up();
            return true;
        }

        case GDK_KEY_q:
        {
            engine->value()->slicing_plane_down();
            return true;
        }

        case GDK_KEY_g:
        {
            GcodeGenerator gcode_generator(
                engine->value()->slice_calculator(),
                engine->value()->printer_config(),
                engine->value()->model->get_model_matrix());

            write_file("./test_all_layers.gcode",
                       gcode_generator.generate_gcode());
            return true;
        }

        // only outputs 1st layer. For testing purposes
        case GDK_KEY_G:
        {
            GcodeGenerator gcode_generator(
                engine->value()->slice_calculator(),
                engine->value()->printer_config(),
                engine->value()->model->get_model_matrix());

            write_file("./test_few_layers.gcode",
                       gcode_generator.generate_gcode({ 10 }));
            return true;
        }

        default: return false;
    }
}