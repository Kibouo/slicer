#include "include/gui/window.hpp"
#include "include/gui/event_handlers.hpp"

typedef struct
{
    gchar const* signal;
    GCallback    handler;
    gint         mask;
    gpointer     params;
} Signal;

void connect_signals(GtkWidget* widget, std::vector<Signal> const& signals)
{
    for (auto& signal : signals)
    {
        gtk_widget_add_events(widget, signal.mask);
        g_signal_connect(widget, signal.signal, signal.handler, signal.params);
    }
}

void connect_window_signals(GtkWindow* window, std::optional<Engine*>& engine)
{
    std::vector<Signal> const signals{
        { "destroy", G_CALLBACK(gtk_main_quit), 0, NULL },
        { "key_press_event", G_CALLBACK(on_key_press), 0, &engine }
    };

    connect_signals(GTK_WIDGET(window), signals);
}

GtkGLArea* build_main_drawing_area(std::optional<Engine*>& engine)
{
    auto const glarea = GTK_GL_AREA(gtk_gl_area_new());

    std::vector<Signal> const signals{
        { "realize", G_CALLBACK(on_realize), 0, &engine },
        { "render", G_CALLBACK(on_main_render), 0, &engine },
        { "resize", G_CALLBACK(on_main_resize), 0, &engine }
    };
    connect_signals(GTK_WIDGET(glarea), signals);

    return glarea;
}

GtkGLArea* build_slice_drawing_area(std::optional<Engine*>& engine)
{
    auto const glarea = GTK_GL_AREA(gtk_gl_area_new());

    std::vector<Signal> const signals{
        { "realize", G_CALLBACK(on_realize), 0, &engine },
        { "render", G_CALLBACK(on_slice_render), 0, &engine },
        { "resize", G_CALLBACK(on_slice_resize), 0, &engine }
    };
    connect_signals(GTK_WIDGET(glarea), signals);

    return glarea;
}

GtkMenuItem* build_menu_item(GtkWindow*              window,
                             std::optional<Engine*>& engine,
                             GtkMenuBar*             menu_bar,
                             std::string const&      name)
{
    auto const title     = name.c_str();
    auto const menu_item = gtk_menu_item_new_with_label(title);

    gtk_menu_shell_append(GTK_MENU_SHELL(menu_bar), menu_item);
    // only 1 param can be passed to callbacks. Instead we use the KV
    // storage on the menu item
    g_object_set_data(G_OBJECT(menu_item), WINDOW_KEY.c_str(), window);
    g_object_set_data(G_OBJECT(menu_item), ENGINE_KEY.c_str(), &engine);

    return GTK_MENU_ITEM(menu_item);
}

GtkMenuBar* build_menu_bar(GtkWindow* window, std::optional<Engine*>& engine)
{
    auto const menu_bar = GTK_MENU_BAR(gtk_menu_bar_new());

    auto const load_stl_menu_item
        = build_menu_item(window, engine, menu_bar, "Load .STL");
    connect_signals(GTK_WIDGET(load_stl_menu_item),
                    { { "activate", G_CALLBACK(on_load_stl), 0, NULL } });

    auto const settings_menu_item
        = build_menu_item(window, engine, menu_bar, "Settings");
    connect_signals(GTK_WIDGET(settings_menu_item),
                    { { "activate", G_CALLBACK(on_edit_settings), 0, NULL } });

    return menu_bar;
}

GtkWindow* build_main_window(std::optional<Engine*>& engine)
{
    auto const window = GTK_WINDOW(gtk_window_new(GTK_WINDOW_TOPLEVEL));
    auto const title  = MAIN_WINDOW_TITLE.c_str();
    gtk_window_set_title(window, title);
    gtk_window_set_default_size(window, DEFAULT_WIDTH, DEFAULT_HEIGHT);
    gtk_widget_add_events(GTK_WIDGET(window), GDK_KEY_PRESS_MASK);
    connect_window_signals(window, engine);

    auto const vertical_layout = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);

    gtk_box_pack_start(GTK_BOX(vertical_layout),
                       GTK_WIDGET(build_menu_bar(window, engine)),
                       false,
                       false,
                       0);
    gtk_box_pack_start(GTK_BOX(vertical_layout),
                       GTK_WIDGET(build_main_drawing_area(engine)),
                       true,
                       true,
                       0);

    gtk_container_add(GTK_CONTAINER(window), vertical_layout);
    return window;
}

GtkWindow* build_slice_window(std::optional<Engine*>& engine)
{
    auto const window = GTK_WINDOW(gtk_window_new(GTK_WINDOW_TOPLEVEL));
    auto const title  = SLICE_WINDOW_TITLE.c_str();
    gtk_window_set_title(window, title);
    // slice is shown as orthogonal projection. We prefer
    // a square window for it
    gtk_window_set_default_size(window, DEFAULT_HEIGHT, DEFAULT_HEIGHT);
    gtk_widget_add_events(GTK_WIDGET(window), GDK_KEY_PRESS_MASK);
    connect_window_signals(window, engine);

    gtk_container_add(GTK_CONTAINER(window),
                      GTK_WIDGET(build_slice_drawing_area(engine)));
    return window;
}

std::vector<GtkWindow*> build_gui(std::optional<Engine*>& engine)
{
    auto const main_window  = build_main_window(engine);
    auto const slice_window = build_slice_window(engine);

    return { main_window, slice_window };
}
