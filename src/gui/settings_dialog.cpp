#include "include/gui/settings_dialog.hpp"
#include "include/engine/infill_type.hpp"
#include <climits>
#include <string>
#include <vector>

GtkSpinButton* build_positive_double_spinbox(double initial_value)
{
    return GTK_SPIN_BUTTON(gtk_spin_button_new(
        gtk_adjustment_new(initial_value, 0, UINT_MAX, 0.001, 0.01, 0), 0, 3));
}

GtkSpinButton* build_uint_spinbox_with_max(uint initial_value, uint max_value)
{
    return GTK_SPIN_BUTTON(gtk_spin_button_new(
        gtk_adjustment_new(initial_value, 0, max_value, 1, 1, 0), 0, 0));
}

GtkSpinButton* build_uint_spinbox(uint initial_value)
{
    return build_uint_spinbox_with_max(initial_value, UINT_MAX);
}

GtkCheckButton* build_checkbox(bool initial_value)
{
    auto const checkbox = GTK_CHECK_BUTTON(gtk_check_button_new());
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkbox), initial_value);
    return checkbox;
}

GtkComboBox* build_dropdown(
    std::vector<std::string> const& possibilities_in_order, int initial_value)
{
    auto const dropdown = GTK_COMBO_BOX_TEXT(gtk_combo_box_text_new());

    for (auto const possibility : possibilities_in_order)
    {
        gtk_combo_box_text_append_text(dropdown, possibility.c_str());
    }
    gtk_combo_box_set_active(GTK_COMBO_BOX(dropdown), initial_value);

    return GTK_COMBO_BOX(dropdown);
}

void add_labeled_input_field_to_container(GtkContainer* container,
                                          std::string   label,
                                          GtkWidget*    input_field)
{
    auto const title             = label.c_str();
    auto const horizontal_layout = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
    gtk_box_pack_start(GTK_BOX(horizontal_layout),
                       GTK_WIDGET(gtk_label_new(title)),
                       true,
                       true,
                       0);
    gtk_box_set_homogeneous(GTK_BOX(horizontal_layout), TRUE);
    gtk_box_pack_start(GTK_BOX(horizontal_layout), input_field, true, true, 0);
    gtk_container_add(container, GTK_WIDGET(horizontal_layout));
}

void add_separator_to_container(GtkContainer* container)
{
    auto const separator
        = GTK_WIDGET(gtk_separator_new(GTK_ORIENTATION_HORIZONTAL));
    gtk_widget_set_margin_top(separator, 10);
    gtk_widget_set_margin_bottom(separator, 10);
    gtk_container_add(container, separator);
}

SettingsDialog::SettingsDialog(GtkWindow*           window,
                               PrinterConfig const& current_config)
{
    this->dialog
        = GTK_DIALOG(gtk_dialog_new_with_buttons("Edit Printer Settings",
                                                 window,
                                                 GTK_DIALOG_MODAL,
                                                 "Cancel",
                                                 GTK_RESPONSE_CANCEL,
                                                 "Accept",
                                                 GTK_RESPONSE_ACCEPT,
                                                 NULL));
    auto const content_area
        = GTK_CONTAINER(gtk_dialog_get_content_area(this->dialog));

    // printer settings
    {
        auto const vertical_layout
            = GTK_CONTAINER(gtk_box_new(GTK_ORIENTATION_VERTICAL, 0));

        this->nozzle_diameter_spinbox
            = build_positive_double_spinbox(current_config.nozzle_diameter);
        add_labeled_input_field_to_container(
            vertical_layout,
            "Nozzle diameter (mm)",
            GTK_WIDGET(this->nozzle_diameter_spinbox));

        this->filament_diameter_spinbox
            = build_positive_double_spinbox(current_config.filament_diameter);
        add_labeled_input_field_to_container(
            vertical_layout,
            "Filament diameter (mm)",
            GTK_WIDGET(this->filament_diameter_spinbox));

        this->width_x_spinbox = build_uint_spinbox(current_config.width_x);
        add_labeled_input_field_to_container(
            vertical_layout, "Width (mm)", GTK_WIDGET(this->width_x_spinbox));

        this->depth_y_spinbox = build_uint_spinbox(current_config.depth_y);
        add_labeled_input_field_to_container(
            vertical_layout, "Depth (mm)", GTK_WIDGET(this->depth_y_spinbox));

        this->height_z_spinbox = build_uint_spinbox(current_config.height_z);
        add_labeled_input_field_to_container(
            vertical_layout, "Height (mm)", GTK_WIDGET(this->height_z_spinbox));

        gtk_container_add(content_area, GTK_WIDGET(vertical_layout));
    }

    add_separator_to_container(content_area);

    // printing settings
    {
        auto const vertical_layout
            = GTK_CONTAINER(gtk_box_new(GTK_ORIENTATION_VERTICAL, 0));

        this->print_bed_temperature_spinbox
            = build_uint_spinbox(current_config.print_bed_temperature);
        add_labeled_input_field_to_container(
            vertical_layout,
            "Print bed temperature (C⁰)",
            GTK_WIDGET(this->print_bed_temperature_spinbox));

        this->printing_temperature_spinbox
            = build_uint_spinbox(current_config.printing_temperature);
        add_labeled_input_field_to_container(
            vertical_layout,
            "Printing temperature (C⁰)",
            GTK_WIDGET(this->printing_temperature_spinbox));

        this->printing_speed_spinbox
            = build_uint_spinbox(current_config.printing_speed);
        add_labeled_input_field_to_container(
            vertical_layout,
            "Printing speed (mm/s)",
            GTK_WIDGET(this->printing_speed_spinbox));

        gtk_container_add(content_area, GTK_WIDGET(vertical_layout));
    }

    add_separator_to_container(content_area);

    // printed settings
    {
        auto const vertical_layout
            = GTK_CONTAINER(gtk_box_new(GTK_ORIENTATION_VERTICAL, 0));

        this->use_supports_checkbox
            = build_checkbox(current_config.use_supports);
        add_labeled_input_field_to_container(
            vertical_layout,
            "Use supports",
            GTK_WIDGET(this->use_supports_checkbox));

        this->infill_density_spinbox
            = build_uint_spinbox_with_max(current_config.infill_density, 100);
        add_labeled_input_field_to_container(
            vertical_layout,
            "Infill density (%)",
            GTK_WIDGET(this->infill_density_spinbox));

        this->infill_type_dropdown = build_dropdown(infill_enum_into_strings(),
                                                    current_config.infill_type);
        add_labeled_input_field_to_container(
            vertical_layout,
            "Infill type",
            GTK_WIDGET(this->infill_type_dropdown));

        this->slice_height_spinbox
            = build_positive_double_spinbox(current_config.slice_height);
        add_labeled_input_field_to_container(
            vertical_layout,
            "Slice height (mm)",
            GTK_WIDGET(this->slice_height_spinbox));

        this->additional_shells_spinbox
            = build_uint_spinbox(current_config.additional_shells);
        add_labeled_input_field_to_container(
            vertical_layout,
            "Additional shells",
            GTK_WIDGET(this->additional_shells_spinbox));

        gtk_container_add(content_area, GTK_WIDGET(vertical_layout));
    }

    gtk_widget_show_all(GTK_WIDGET(this->dialog));
}

SettingsDialog::~SettingsDialog()
{
    if (dialog) { gtk_widget_destroy(GTK_WIDGET(this->dialog)); }
}

int SettingsDialog::run() const { return gtk_dialog_run(this->dialog); }

std::pair<std::optional<PrinterConfig>, std::optional<std::string>>
SettingsDialog::try_into_printer_config() const
{
    PrinterConfig const printer_config = {
        gtk_spin_button_get_value(this->nozzle_diameter_spinbox),
        gtk_spin_button_get_value(this->filament_diameter_spinbox),
        (uint)gtk_spin_button_get_value_as_int(this->width_x_spinbox),
        (uint)gtk_spin_button_get_value_as_int(this->depth_y_spinbox),
        (uint)gtk_spin_button_get_value_as_int(this->height_z_spinbox),

        (uint)gtk_spin_button_get_value_as_int(
            this->print_bed_temperature_spinbox),
        (uint)gtk_spin_button_get_value_as_int(
            this->printing_temperature_spinbox),
        (uint)gtk_spin_button_get_value_as_int(this->printing_speed_spinbox),

        (bool)gtk_toggle_button_get_active(
            GTK_TOGGLE_BUTTON(this->use_supports_checkbox)),
        (uint)gtk_spin_button_get_value_as_int(this->infill_density_spinbox),
        static_cast<InfillType>(
            gtk_combo_box_get_active(this->infill_type_dropdown)),
        gtk_spin_button_get_value(this->slice_height_spinbox),
        (uint)gtk_spin_button_get_value_as_int(this->additional_shells_spinbox),
    };

    if (printer_config.nozzle_diameter * 0.8 < printer_config.slice_height)
    {
        // we want to write "%" but gtk interprets this as string formatting and
        // inserts garbage data. Escaping it doesn't work.
        return {
            std::nullopt,
            "Slicing thickness may not be more than 80 percent of nozzle diameter!"
        };
    }

    return { printer_config, std::nullopt };
}