#include "include/engine/engine.hpp"
#include "include/engine/slice.hpp"
#include "include/engine/slice_calculator.hpp"
#include <assimp/mesh.h>
#include <glm/mat4x4.hpp>

Engine::Engine()
{
    this->model_program = std::make_unique<ShaderProgram>(
        "resources/model.glslv", "resources/model.glslf");
    this->model_camera = std::make_unique<PerspectiveCamera>(
        this->model_program->uniform_location("view_matrix"),
        this->model_program->uniform_location("projection_matrix"));

    this->model = std::make_unique<Model>(
        glm::mat4(1.0f),
        this->model_program->uniform_location("model_matrix"),
        this->model_program->uniform_location("color"),
        std::vector<Vertex>{},
        Color{ 1.0, 1.0, 1.0, 1.0 },
        "");

    this->slice_program = std::make_unique<ShaderProgram>(
        "resources/slice.glslv", "resources/slice.glslf");
    this->slice_camera = std::make_unique<OrthographicCamera>(
        this->slice_program->uniform_location("view_matrix"),
        this->slice_program->uniform_location("projection_matrix"));
    this->update_printer_config(PrinterConfig());

    this->slice_plane = std::make_unique<SlicePlane>(
        glm::mat4(1.0f),
        this->model_program->uniform_location("model_matrix"),
        this->model_program->uniform_location("color"),
        this->_slice_calculator->get_slice_height(
            this->model->slicing_plane()));

    // TODO: comment out for production. This is useful for development as it's
    // annoying to load model each time...
    this->load_model(
        std::filesystem::path("resources/models/support-test.stl"));
}

void Engine::draw_model() const
{
    this->model_program->use();

    this->model_camera->use();
    this->model->draw();

    this->slice_plane->draw();
}

void Engine::draw_slice() const
{
    this->slice_program->use();

    this->slice_camera->use();
    auto const& slice
        = this->_slice_calculator->get_slice(this->model->slicing_plane());
    if (slice.has_value()) { slice.value().draw(); }
}

void Engine::update_printer_config(PrinterConfig const& printer_config)
{
    this->_printer_config   = printer_config;
    this->_slice_calculator = std::make_unique<SliceCalculator>(
        this->_printer_config.nozzle_diameter,
        this->_printer_config.use_supports,
        this->_printer_config.infill_density,
        this->_printer_config.infill_type,
        this->_printer_config.slice_height,
        this->_printer_config.additional_shells,
        this->slice_program->uniform_location("color"),
        this->slice_program->uniform_location("model_matrix"));
    this->_slice_calculator->calculateSlices(this->model);
}

void Engine::load_model(std::filesystem::path const& stl_file)
{
    this->model
        = Model::from_stl(stl_file,
                          this->model_program->uniform_location("model_matrix"),
                          this->model_program->uniform_location("color"));
    this->_slice_calculator->calculateSlices(this->model);
}

void Engine::slicing_plane_up()
{
    this->model->slicing_plane_up();
    this->slice_plane->update_z_layer(this->_slice_calculator->get_slice_height(
        this->model->slicing_plane()));
}

void Engine::slicing_plane_down()
{
    this->model->slicing_plane_down();
    this->slice_plane->update_z_layer(this->_slice_calculator->get_slice_height(
        this->model->slicing_plane()));
}