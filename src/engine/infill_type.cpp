#include "include/engine/infill_type.hpp"

std::string infill_type_into_string(InfillType const type)
{
    switch (type)
    {
        case InfillType::Rectangular: return "Rectangular";
        case InfillType::Triangles: return "Triangles";
        case InfillType::Lines: return "Lines";
        case InfillType::Cubic: return "Cubic";
    }
}

std::vector<std::string> infill_enum_into_strings()
{
    std::vector const        possibilities_in_order{ InfillType::Rectangular,
                                              InfillType::Triangles,
                                              InfillType::Lines,
                                              InfillType::Cubic };
    std::vector<std::string> result{};

    for (auto const possibility : possibilities_in_order)
    {
        result.push_back(infill_type_into_string(possibility));
    }

    return result;
}