#include "include/engine/slice.hpp"
#include "clipper.hpp"
#include "fmt/core.h"
#include "include/utils.hpp"
#include <iostream>

auto const PERIMETER_COLOR  = glm::vec3{ 1.0, 0.0, 0.0 };    // red
auto const SHELL_COLOR      = glm::vec3{ 0.0, 1.0, 1.0 };    // turquoise
auto const INFILL_COLOR     = glm::vec3{ 1.0, 1.0, 0.0 };    // yellow
auto const ROOF_FLOOR_COLOR = glm::vec3{ 0.0, 1.0, 0.0 };    // green
auto const SUPPORT_COLOR    = glm::vec3{ 1.0, 0.3, 1.0 };    // pink

auto const MAX_EPSILON_FACTOR = INT_POINT_PREC * 0.01;

ClipperLib::IntPoint point2d_into_int_point(Point2D const& point)
{
    return ClipperLib::IntPoint(point.x * INT_POINT_PREC,
                                point.y * INT_POINT_PREC);
}

ClipperLib::Paths vec_of_point2ds_into_paths(
    std::vector<std::vector<Point2D>> const vec_of_points)
{
    ClipperLib::Paths resulting_paths(vec_of_points.size());
    std::transform(
        vec_of_points.begin(),
        vec_of_points.end(),
        resulting_paths.begin(),
        [](std::vector<Point2D> points) {
            std::vector<ClipperLib::IntPoint> resulting_path(points.size());
            std::transform(points.begin(),
                           points.end(),
                           resulting_path.begin(),
                           [](Point2D p) { return point2d_into_int_point(p); });
            return resulting_path;
        });

    return resulting_paths;
}

std::vector<std::vector<Point2D>> filter_away_empty_paths(
    std::vector<std::vector<Point2D>> const partial_paths)
{
    std::vector<std::vector<Point2D>> left_over{};
    for (auto const& path : partial_paths)
    {
        if (path.size() != 0) { left_over.push_back(path); }
    }
    return left_over;
}

// keep trying head-to-tail method on left-over partial paths. The epsilon for
// equality keeps getting increased till all partial paths are combined into
// closed paths.
std::vector<std::vector<Point2D>> combine_partial_paths(
    std::vector<std::vector<Point2D>> partial_paths,
    float const                       epsilon_factor = 1.0)
{
    std::vector<std::vector<Point2D>> closed_paths{};    // result

    for (auto& partial_path : partial_paths)
    {
        // skip "deleted" item
        if (partial_path.size() == 0) { continue; }

        for (auto& other_partial_path : partial_paths)
        {
            // skip "deleted" item
            if (other_partial_path.size() == 0) { continue; }

            // could check whether `partial_path` and `other_partial_path` are
            // the same and, if so, skip. However, we don't perform any more
            // work inside this loop, aside from the following head/tails check.
            // Given that partial paths' head and tail are not the same (per
            // definition: a partial path is an open path), this following
            // check is equal to a `partial_path == other_partial_path` check.
            if (partial_path.back().equals(other_partial_path.front(),
                                           epsilon_factor)
                || partial_path.front().equals(other_partial_path.back(),
                                               epsilon_factor))
            {
                if (partial_path.back().equals(other_partial_path.front(),
                                               epsilon_factor))
                {
                    partial_path.insert(
                        partial_path.end(),
                        std::make_move_iterator(other_partial_path.begin()),
                        std::make_move_iterator(other_partial_path.end()));
                }
                else
                {
                    partial_path.insert(
                        partial_path.begin(),
                        std::make_move_iterator(other_partial_path.begin()),
                        std::make_move_iterator(other_partial_path.end()));
                }

                if (partial_path.front().equals(partial_path.back(),
                                                epsilon_factor))
                {
                    closed_paths.push_back(
                        { partial_path.begin(), partial_path.end() });
                    partial_path = {};    // "delete" item
                }

                // don't delete earlier as `partial_path` and
                // `other_partial_path` may point to the same item
                other_partial_path = {};    // "delete" item
            }
        }
    }

    auto left_over_partial_paths
        = filter_away_empty_paths(std::move(partial_paths));
    // some partial paths could not be combined. We easy the epsilon for
    // comparison and keep trying to close all paths.
    // Only keep going up to a certain epsilon to prevent infinite recursion.
    if (left_over_partial_paths.size() != 0
        && epsilon_factor < MAX_EPSILON_FACTOR)
    {
        auto const extra_closed_paths = combine_partial_paths(
            std::move(left_over_partial_paths), epsilon_factor * 1.5);
        closed_paths.insert(closed_paths.end(),
                            std::make_move_iterator(extra_closed_paths.begin()),
                            std::make_move_iterator(extra_closed_paths.end()));
    }

    return closed_paths;
}

// Combine segments with head-to-tail method to group them into
// multiple paths (1 path per polygon in slice).
// `slicing_edges` is a list of unordered edges.
// TODO: could this be combined with `combine_partial_paths`?
ClipperLib::Paths build_closed_paths(
    std::vector<std::array<Point2D, 2>> const slicing_edges)
{
    std::vector<std::vector<Point2D>> partial_paths(slicing_edges.size());
    std::transform(slicing_edges.begin(),
                   slicing_edges.end(),
                   partial_paths.begin(),
                   [](std::array<Point2D, 2> segment) {
                       return std::vector{ segment[0], segment[1] };
                   });

    return vec_of_point2ds_into_paths(
        combine_partial_paths(std::move(partial_paths)));
}

ClipperLib::Paths compile_paths(ClipperLib::Paths const partial_paths)
{
    // A contour may exist of multiple parts (closed paths). Clipper will merge
    // them
    // for us. E.g. slicing edges may form following following partial contours:
    /*
     idx 0         idx n
     o---o             o
     |  /    ...     / |
     | /            /  |
     o             o---o
    */
    ClipperLib::Clipper clipper;

    clipper.AddPaths(partial_paths, ClipperLib::PolyType::ptSubject, true);

    ClipperLib::Paths solution;
    clipper.Execute(ClipperLib::ClipType::ctUnion,
                    solution,
                    ClipperLib::PolyFillType::pftEvenOdd,
                    ClipperLib::PolyFillType::pftEvenOdd);
    // CleanPolygons(solution);

    return solution;
}

Slice::Slice(uint const                                layer_idx,
             std::vector<std::array<Point2D, 2>> const slicing_edges,
             double const                              nozzle_diameter,
             ClipperLib::IntRect const                 bounds,
             glm::mat4 const                           model_matrix,
             GLint const                               color_handle,
             GLint const                               model_matrix_handle)
    : layer_idx(layer_idx)
    , nozzle_diameter(nozzle_diameter)
    , bounds(bounds)
    , model_matrix(model_matrix)
    , color_handle(color_handle)
    , model_matrix_handle(model_matrix_handle)
{
    // init OpenGL stuff
    glGenVertexArrays(1, &this->vao);
    glBindVertexArray(this->vao);

    glGenBuffers(1, &this->vertex_buffer);

    this->perimeters
        = compile_paths(build_closed_paths(std::move(slicing_edges)));
}

void Slice::draw_paths(ClipperLib::Paths const& paths,
                       glm::vec3 const          color,
                       bool const               paths_are_closed) const
{
    for (auto const& path : paths)
    {
        glBindVertexArray(this->vao);
        glBindBuffer(GL_ARRAY_BUFFER, this->vertex_buffer);

        glUniform3fv(this->color_handle, 1, &color.r);
        glUniformMatrix4fv(
            this->model_matrix_handle, 1, GL_FALSE, &this->model_matrix[0][0]);

        glBufferData(GL_ARRAY_BUFFER,
                     sizeof(Point2D) * path.size(),
                     path_to_points(path, paths_are_closed).data(),
                     GL_STATIC_DRAW);

        glBindVertexArray(this->vao);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, NULL);

        if (paths_are_closed) { glDrawArrays(GL_LINE_LOOP, 0, path.size()); }
        else
        {
            glDrawArrays(GL_LINES, 0, path.size());
        }
        glDisableVertexAttribArray(0);
    }
}

Slice::~Slice()
{
    // glDeleteBuffers(1, &this->vertex_buffer);
    // glDeleteVertexArrays(1, &this->vao);
}

void Slice::draw() const
{
    this->draw_paths(this->get_perimeters(), PERIMETER_COLOR, true);
    for (auto const& shell : this->get_shells())
    {
        this->draw_paths(shell, SHELL_COLOR, true);
    }
    this->draw_paths(this->get_infill(), INFILL_COLOR, false);
    this->draw_paths(
        this->get_floor().get_filled_out_shell(), ROOF_FLOOR_COLOR, false);
    this->draw_paths(
        this->get_roof().get_filled_out_shell(), ROOF_FLOOR_COLOR, false);
    this->draw_paths(
        this->get_support().get_support_structure(), SUPPORT_COLOR, false);
}

std::string Slice::to_string() const
{
    std::string result = "";
    for (auto const& perimeter : this->get_perimeters())
    {
        for (auto const& p : perimeter)
        {
            result += fmt::v7::format("{},{}\n", p.X, p.Y);
        }
        result += "\n";
    }
    return result;
}

void Slice::erode_slice()
{
    this->perimeters
        = offset(this->get_perimeters(), -this->nozzle_diameter / 2.0);
}

void Slice::create_shells(uint const amount)
{
    // generate shells from inside to out. This makes generating gcode in the
    // right order easier.
    for (uint i = amount; i > 0; i--)
    {
        double const offset_amount = -this->nozzle_diameter * double(i);

        ClipperLib::Paths shell = offset(this->get_perimeters(), offset_amount);
        this->shells.push_back(shell);
    }
}

void Slice::generate_infill(uint8_t const    infill_density_percentage,
                            InfillType const type)
{
    ClipperLib::Clipper clipper;
    // only the innermost shell should be considered. Other shells already have
    // no space in-between them.
    clipper.AddPaths(
        this->get_innermost_shell(), ClipperLib::PolyType::ptSubject, true);
    // subtract already filled areas (the floor/roof)
    clipper.AddPaths(
        this->get_floor().get_contour(), ClipperLib::PolyType::ptClip, true);
    clipper.AddPaths(
        this->get_roof().get_contour(), ClipperLib::PolyType::ptClip, true);

    ClipperLib::Paths fillable_area;
    clipper.Execute(ClipperLib::ClipType::ctDifference,
                    fillable_area,
                    ClipperLib::PolyFillType::pftEvenOdd,
                    ClipperLib::PolyFillType::pftEvenOdd);

    // start filling
    clipper.Clear();
    clipper.AddPaths(fillable_area, ClipperLib::PolyType::ptClip, true);

    auto const infill_structure
        = generate_infill_structure(this->nozzle_diameter,
                                    layer_idx,
                                    this->bounds,
                                    infill_density_percentage,
                                    type);

    clipper.AddPaths(infill_structure, ClipperLib::PolyType::ptSubject, false);

    ClipperLib::PolyTree tree_solution;
    clipper.Execute(ClipperLib::ClipType::ctIntersection,
                    tree_solution,
                    ClipperLib::PolyFillType::pftEvenOdd,
                    ClipperLib::PolyFillType::pftEvenOdd);

    PolyTreeToPaths(tree_solution, this->infill);
}

ClipperLib::Paths const& Slice::get_innermost_shell() const
{
    if (this->get_shells().size() > 0) { return this->get_shells().front(); }
    else
    {
        return this->get_perimeters();
    }
}

ClipperLib::Paths Slice::build_horizontal_shell_contour(
    std::optional<Slice> const& other_slice, uint const amount_shells) const
{
    ClipperLib::Clipper clipper;
    // only the innermost shell should be considered. Other shells already
    // have no space in-between them.
    clipper.AddPaths(
        this->get_innermost_shell(), ClipperLib::PolyType::ptSubject, true);

    if (other_slice.has_value())
    {
        clipper.AddPaths(other_slice.value().get_innermost_shell(),
                         ClipperLib::PolyType::ptClip,
                         true);
    }

    ClipperLib::Paths contour;
    clipper.Execute(ClipperLib::ClipType::ctDifference,
                    contour,
                    ClipperLib::PolyFillType::pftEvenOdd,
                    ClipperLib::PolyFillType::pftEvenOdd);

    return contour;
}

void Slice::build_floor(std::optional<Slice> const& prev_slice,
                        uint const                  amount_shells)
{
    auto const contour
        = this->build_horizontal_shell_contour(prev_slice, amount_shells);
    if (prev_slice.has_value())
    {
        this->floor = HorizontalShell(prev_slice.value().get_floor(), contour);
    }
    else
    {
        this->floor
            = HorizontalShell(this->nozzle_diameter, amount_shells, contour);
    }
}

void Slice::build_roof(std::optional<Slice> const& next_slice,
                       uint const                  amount_shells)
{
    auto const contour
        = this->build_horizontal_shell_contour(next_slice, amount_shells);
    if (next_slice.has_value())
    {
        this->roof = HorizontalShell(next_slice.value().get_roof(), contour);
    }
    else
    {
        this->roof
            = HorizontalShell(this->nozzle_diameter, amount_shells, contour);
    }
}

void Slice::generate_supports(std::optional<Slice> const& next_slice)
{
    ClipperLib::Clipper clipper;
    if (next_slice.has_value())
    {
        clipper.AddPaths(next_slice.value().get_perimeters(),
                         ClipperLib::PolyType::ptSubject,
                         true);
    }
    else
    {
        clipper.AddPaths({}, ClipperLib::PolyType::ptSubject, true);
    }

    auto const dilated_perimeters
        = offset(this->get_perimeters(), this->nozzle_diameter / 2.0);
    clipper.AddPaths(dilated_perimeters, ClipperLib::PolyType::ptClip, true);

    ClipperLib::IntRect model_bounds;
    if (!next_slice.has_value()) { model_bounds = clipper.GetBounds(); }

    ClipperLib::Paths to_be_supported_area;
    clipper.Execute(ClipperLib::ClipType::ctDifference,
                    to_be_supported_area,
                    ClipperLib::PolyFillType::pftEvenOdd,
                    ClipperLib::PolyFillType::pftEvenOdd);

    if (next_slice.has_value())
    {
        this->support = SupportLayer(next_slice.value().get_support(),
                                     this->get_perimeters(),
                                     to_be_supported_area);
    }
    else
    {
        this->support = SupportLayer(
            this->nozzle_diameter, model_bounds, to_be_supported_area);
    }
}
