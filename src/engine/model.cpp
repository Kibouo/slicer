#include "include/engine/model.hpp"
#include "include/engine/slice_calculator.hpp"
#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>
#include <assimp/scene.h>
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <iostream>

std::unique_ptr<Model> Model::from_stl(std::filesystem::path const& file,
                                       GLint const model_matrix_handle,
                                       GLint const color_handle)
{
    auto       importer = Assimp::Importer();
    auto const scene    = importer.ReadFile(file,
                                         aiProcess_Triangulate
                                             | aiProcess_ImproveCacheLocality
                                             | aiProcess_GenBoundingBoxes);
    if (!scene)
    {
        std::cerr << "Failed to load " << file << "\n"
                  << importer.GetErrorString() << std::endl;
    }

    // STL files contain only 1 mesh
    auto const          mesh = scene->mMeshes[0];
    std::vector<Vertex> vertices((Vertex*)mesh->mVertices,
                                 (Vertex*)mesh->mVertices + mesh->mNumVertices);

    // make sure object's X & Y are centered around 0, and Z starts at 0
    // (printing plate height).
    auto const model_width = mesh->mAABB.mMax.x - mesh->mAABB.mMin.x;
    auto const model_depth = mesh->mAABB.mMax.y - mesh->mAABB.mMin.y;
    auto const model_matrix
        = glm::translate(glm::mat4(1.0f),
                         glm::vec3(-mesh->mAABB.mMin.x - model_width / 2.0,
                                   -mesh->mAABB.mMin.y - model_depth / 2.0,
                                   -mesh->mAABB.mMin.z));

    // 1 mesh means only 1 material
    // only use diffuse. We're not planning to add fancy lighting, etc.
    auto const material = scene->mMaterials[0];
    aiColor4D  diffuse;
    if (AI_SUCCESS
        == aiGetMaterialColor(material, AI_MATKEY_COLOR_DIFFUSE, &diffuse))
    {
        return std::make_unique<Model>(
            model_matrix,
            model_matrix_handle,
            color_handle,
            std::move(vertices),
            Color{ diffuse.r, diffuse.g, diffuse.b, diffuse.a },
            file);
    }
    else
    {
        return std::make_unique<Model>(model_matrix,
                                       model_matrix_handle,
                                       color_handle,
                                       std::move(vertices),
                                       Color{ 1.0, 1.0, 1.0, 1.0 },
                                       file);
    }
}

Model::Model(glm::mat4 const              model_matrix,
             GLint const                  model_matrix_handle,
             GLint const                  color_handle,
             std::vector<Vertex> const    vertices,
             Color const                  color,
             std::filesystem::path const& source_file)
    : vertices(vertices)
    , model_matrix(model_matrix)
    , color(color)
    , _slicing_plane(0)
    , model_matrix_handle(model_matrix_handle)
    , color_handle(color_handle)
    , _source_file(source_file)
{
    // init OpenGL stuff
    glGenVertexArrays(1, &this->vao);
    glBindVertexArray(this->vao);

    glGenBuffers(1, &this->vertex_buffer);
    glBindBuffer(GL_ARRAY_BUFFER, this->vertex_buffer);
    glBufferData(GL_ARRAY_BUFFER,
                 sizeof(Vertex) * this->vertices.size(),
                 this->vertices.data(),
                 GL_STATIC_DRAW);
}

void Model::draw() const
{
    glBindVertexArray(this->vao);
    glBindBuffer(GL_ARRAY_BUFFER, this->vertex_buffer);

    glUniform4fv(this->color_handle, 1, &this->color.r);
    glUniformMatrix4fv(
        this->model_matrix_handle, 1, GL_FALSE, &this->model_matrix[0][0]);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    glDrawArrays(GL_TRIANGLES, 0, this->vertices.size() * 3);
    glDisableVertexAttribArray(0);
}

Model::~Model()
{
    // glDeleteVertexArrays(1, &this->vao);
    // glDeleteBuffers(1, &this->vertex_buffer);
}

void Model::slicing_plane_up()
{
    this->_slicing_plane += 1;
    std::cout << "Selected slicing plane nr." << this->_slicing_plane
              << std::endl;
}

void Model::slicing_plane_down()
{
    if (this->_slicing_plane > 0) { this->_slicing_plane -= 1; }
    std::cout << "Selected slicing plane nr." << this->_slicing_plane
              << std::endl;
}
