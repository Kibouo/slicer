#include "include/engine/slice_calculator.hpp"
#include "clipper.hpp"
#include "include/engine/model.hpp"
#include "include/engine/slice.hpp"
#include "include/utils.hpp"

#include <assimp/Importer.hpp>
#include <assimp/mesh.h>
#include <assimp/postprocess.h>
#include <assimp/scene.h>
#include <glm/vec3.hpp>
#include <iostream>
#include <numeric>
#include <optional>

glm::vec3 const SLICE_PLANE_NORMAL{ 0.0, 0.0, 1.0 };

uint layerCount(float model_height, float layer_thickness)
{
    uint count = model_height / layer_thickness;
    std::cout << "Layers: " << count << std::endl;
    return count;
}

// based on
// https://stackoverflow.com/questions/5666222/3d-line-plane-intersection
std::optional<glm::vec3> intersection_point_plane_with_line(
    glm::vec3 const& point_on_plane,
    glm::vec3 const& plane_normal,
    glm::vec3 const& point_on_line,
    glm::vec3 const& line_direction)
{
    // line is parallel to plane -> no single intersection to report
    if (glm::dot(plane_normal, glm::normalize(line_direction)) == 0)
    {
        return std::nullopt;
    }

    auto const t = (glm::dot(plane_normal, point_on_plane)
                    - glm::dot(plane_normal, point_on_line))
                   / glm::dot(plane_normal, glm::normalize(line_direction));
    return { point_on_line + (glm::normalize(line_direction) * t) };
}

SliceCalculator::SliceCalculator(double const     nozzle_diameter,
                                 bool const       use_supports,
                                 uint const       infill_density,
                                 InfillType const infill_type,
                                 double const     slice_height,
                                 uint const       additional_shells,
                                 GLint const      color_handle,
                                 GLint const      model_matrix_handle)
    : nozzle_diameter(nozzle_diameter)
    , use_supports(use_supports)
    , infill_density(infill_density)
    , infill_type(infill_type)
    , slice_height(slice_height)
    , additional_shells(additional_shells)
    , color_handle(color_handle)
    , model_matrix_handle(model_matrix_handle)
{
}

void SliceCalculator::calculateSlices(std::unique_ptr<Model> const& model)
{
    std::cout << "Calculating slices" << std::endl;
    this->_slices.clear();

    auto       importer = Assimp::Importer();
    auto const scene    = importer.ReadFile(model->get_source_file(),
                                         aiProcess_Triangulate
                                             | aiProcess_ImproveCacheLocality
                                             | aiProcess_GenBoundingBoxes);
    if (!scene) return;
    // constant as we asked Assimp to triangulate
    auto const vertices_per_face = 3;

    auto const mesh       = scene->mMeshes[0];
    auto const amt_layers = layerCount(mesh->mAABB.mMax.z - mesh->mAABB.mMin.z,
                                       this->slice_height);
    for (uint layer_idx = 0; layer_idx < amt_layers; layer_idx++)
    {
        // add half a slice height such that slices are calculated in the middle
        // of a layer
        double z_layer = (double(layer_idx) + 0.5) * this->slice_height;
        // compensate for vertices potentially being under z. The rendering
        // steps use a model matrix for this. We could do that as well, but
        // multiplying a whole matrix is more work.
        z_layer += mesh->mAABB.mMin.z;

        std::vector<std::array<Point2D, 2>> slicing_edges{};

        // We're now gonna find faces that cut the slicing plane, select the 2
        // edges that do the actual intersecting, and use these to calculate a
        // part of the slicing plane's contour.
        // Each contour must have a consistent direction in which the edges are
        // stored (direction depends on whether it's a shell or a hole). E.g.: a
        // triangle with 3 points would have following edges stored: `[0 --> 1]
        // [1 --> 2] [2 --> 0]`. To achieve a consistent order in the generated
        // edges, we must watch out how these edges are constructed. Take
        // following example of 2 faces (order of vertices being stored is
        // indicated by arrow, open edge is implicit):
        /*
            o    o    o
            |   /    /
        --------------------------- slicing plane
            v /    /
            o    o --> o

        Example 1 */
        // A way to get the 2 edges that intersect the plane is to
        // simply take the standalone vertex of each face and
        // connect it to the other 2 vertices of the face, in order
        // of them being stored. For the 1st example this would give
        // following (numbered) points:
        /*
            o    o    o
            |   /    /
        ----0--1----2--3----------- slicing plane
            v /    /
            o    o --> o
        */
        // But this can go wrong! For example if the faces would be
        // stored as follows:
        /*
            o <-- o   o
                /    /
        --------------------------- slicing plane
              /    /
            o    o --> o
        Example 2 */
        // This would give the points in following order:
        /*
            o <-- o   o
                /    /
        ----1--0----2--3----------- slicing plane
              /    /
            o    o --> o
        */
        // The order of points would cause edges to point in opposite
        // directions. Clearly not what we want! To ensure a consistent
        // direction, we must choose the intersecting edges in a consistent
        // order. We make sure the edge which goes from under to above the
        // slicing plane is put 1st, and the edge that goes from above to under
        // as 2nd.
        for (size_t f = 0; f < mesh->mNumFaces; f++)
        {
            std::array<Point2D, 2> slicing_contour_edge;
            auto                   face_intersects_with_slicing_plane = false;

            auto const face = mesh->mFaces[f];
            for (auto vertex_idx = 0; vertex_idx < vertices_per_face;
                 vertex_idx += 1)
            {
                auto const next_vertex_idx
                    = (vertex_idx + 1) % vertices_per_face;

                auto const& vertex = mesh->mVertices[face.mIndices[vertex_idx]];
                auto const& next_vertex
                    = mesh->mVertices[face.mIndices[next_vertex_idx]];

                auto const vertex_under_slicing_plane
                    = double(vertex.z) < z_layer;
                auto const next_vertex_under_slicing_plane
                    = double(next_vertex.z) < z_layer;

                if (vertex_under_slicing_plane
                    != next_vertex_under_slicing_plane)
                {
                    face_intersects_with_slicing_plane = true;

                    // Calculate instersection point of the edge passing
                    // through the slicing plane.
                    glm::vec3 const point_on_line{ vertex.x,
                                                   vertex.y,
                                                   vertex.z };
                    auto const      intersect_point
                        = intersection_point_plane_with_line(
                            { 0.0, 0.0, z_layer },
                            SLICE_PLANE_NORMAL,
                            point_on_line,
                            point_on_line
                                - glm::vec3{ next_vertex.x,
                                             next_vertex.y,
                                             next_vertex.z });

                    if (!intersect_point.has_value())
                    {
                        throw std::runtime_error(
                            "We determined that the 2 vertices were on opposite sides of the slicing plane. Yet, the intersection calculation says the edge is parallel to the slicing plane...");
                    }

                    // the edge which starts under the slicing plane and ends
                    // above it produces the 1st intersection point
                    if (vertex_under_slicing_plane)
                    {
                        slicing_contour_edge[0] = { intersect_point.value().x,
                                                    intersect_point.value().y };
                    }
                    // other way around produces the 2nd intersection point
                    else
                    {
                        slicing_contour_edge[1] = { intersect_point.value().x,
                                                    intersect_point.value().y };
                    }
                }
            }

            if (face_intersects_with_slicing_plane)
            {
                slicing_edges.push_back(slicing_contour_edge);
            }
        }

        Slice slice(layer_idx,
                    std::move(slicing_edges),
                    this->nozzle_diameter,
                    { (ClipperLib::cInt) (mesh->mAABB.mMin.x * INT_POINT_PREC),
                      (ClipperLib::cInt) (mesh->mAABB.mMax.y * INT_POINT_PREC),
                      (ClipperLib::cInt) (mesh->mAABB.mMax.x * INT_POINT_PREC),
                      (ClipperLib::cInt) (mesh->mAABB.mMin.y * INT_POINT_PREC)},
                    model->get_model_matrix(),
                    this->color_handle,
                    this->model_matrix_handle);
        slice.erode_slice();
        slice.create_shells(this->additional_shells);
        slice.build_floor(this->_slices.size() != 0
                              ? std::optional{ this->_slices.back() }
                              : std::nullopt,
                          this->additional_shells);
        this->_slices.push_back(std::move(slice));
    }

    // building a roof and supports depends on the roof/support of the slice
    // above it. This means we gotta iterate over the slices again, but now from
    // top to bottom.
    for (int slice_idx = this->_slices.size() - 1; slice_idx >= 0;
         slice_idx -= 1)
    {
        auto& slice = this->_slices[slice_idx];

        slice.build_roof(slice_idx < this->_slices.size() - 1
                             ? std::optional{ this->_slices[slice_idx + 1] }
                             : std::nullopt,
                         this->additional_shells);

        if (this->use_supports)
        {
            slice.generate_supports(
                slice_idx < this->_slices.size() - 1
                    ? std::optional{ this->_slices[slice_idx + 1] }
                    : std::nullopt);
        }

        slice.generate_infill(this->infill_density, this->infill_type);
    }
}
