#include "include/engine/camera/orthographic_camera.hpp"
#include "include/gui/window.hpp"

#include <glm/gtc/matrix_transform.hpp>

float const LEFT   = -75.0;
float const RIGHT  = 75.0;
float const BOTTOM = -75.0;
float const TOP    = 75.0;

OrthographicCamera::OrthographicCamera(GLint const view_matrix_handle,
                                       GLint const projection_matrix_handle)
    : Camera(view_matrix_handle, projection_matrix_handle)
{
    this->projection_matrix = glm::ortho(LEFT, RIGHT, BOTTOM, TOP, ZNEAR, ZFAR);
    this->view_matrix       = glm::lookAt(
        glm::vec3(0, ZNEAR, 100), glm::vec3(0, 0, 0), glm::vec3(0, 0, -1));
}

void OrthographicCamera::aspect_ratio(uint const width, uint const height)
{
    auto const ratio = (float)width / (float)height;
    this->projection_matrix
        = glm::ortho(LEFT * ratio, RIGHT * ratio, BOTTOM, TOP, ZNEAR, ZFAR);
}