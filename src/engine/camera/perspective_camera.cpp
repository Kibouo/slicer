#include "include/engine/camera/perspective_camera.hpp"
#include "include/gui/window.hpp"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/trigonometric.hpp>

float const FOV = glm::radians(45.0f);

PerspectiveCamera::PerspectiveCamera(GLint const view_matrix_handle,
                                     GLint const projection_matrix_handle)
    : Camera(view_matrix_handle, projection_matrix_handle)
{
    this->projection_matrix = glm::perspective(
        FOV, float(DEFAULT_WIDTH) / float(DEFAULT_HEIGHT), ZNEAR, ZFAR);
    this->view_matrix = glm::lookAt(
        glm::vec3(100, 100, 100), glm::vec3(0, 0, 10), glm::vec3(0, 0, 1));
}

void PerspectiveCamera::aspect_ratio(uint const width, uint const height)
{
    this->projection_matrix
        = glm::perspective(FOV, float(width) / float(height), ZNEAR, ZFAR);
}