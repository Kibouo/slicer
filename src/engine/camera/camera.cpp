#include "include/engine/camera/camera.hpp"

#ifdef __APPLE__
//#include <OpenGL/gl.h>
#define GL_SILENCE_DEPRECATION
#include <OpenGL/gl3.h>
#else
#include <GL/gl.h>
#endif

Camera::Camera(GLint const view_matrix_handle,
               GLint const projection_matrix_handle)
    : view_matrix_handle(view_matrix_handle)
    , projection_matrix_handle(projection_matrix_handle)
{
}

void Camera::use() const
{
    glUniformMatrix4fv(
        this->view_matrix_handle, 1, GL_FALSE, &this->view_matrix[0][0]);
    glUniformMatrix4fv(this->projection_matrix_handle,
                       1,
                       GL_FALSE,
                       &this->projection_matrix[0][0]);
}
