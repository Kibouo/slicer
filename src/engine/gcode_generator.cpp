#include "include/engine/gcode_generator.hpp"
#include "fmt/core.h"
#include "include/engine/slice_calculator.hpp"
#include "include/utils.hpp"
#include <algorithm>
#include <cmath>
#include <optional>

GcodeGenerator::GcodeGenerator(
    std::shared_ptr<SliceCalculator> const slice_calculator,
    PrinterConfig const&                   printer_config,
    glm::mat4 const&                       model_matrix)
    : slice_calculator(slice_calculator)
    , printer_config(printer_config)
    , filament_area(M_PI
                    * pow(this->printer_config.filament_diameter / 2.0, 2.0))
{
    Point2D const model_offset = { model_matrix[3][0], model_matrix[3][1] };
    Point2D const print_bed_center
        = { float(this->printer_config.width_x / 2),
            float(this->printer_config.depth_y / 2) };
    this->print_offset = model_offset + print_bed_center;
}

void GcodeGenerator::set_feedrate(float const speed_up_factor)
{
    this->instructions += fmt::v7::format(
        "G1 F{}\n", this->printer_config.printing_speed * 60 * speed_up_factor);
}

void GcodeGenerator::move_z(uint const slice_idx)
{
    this->instructions += fmt::v7::format(
        "G1 Z{}\n", this->printer_config.slice_height * double(slice_idx + 1));
}

std::string GcodeGenerator::generate_gcode(std::optional<uint> const amt_slices)
{
    this->instructions
        += fmt::v7::format(PRE_HEAT_TEMPLATE,
                           this->printer_config.print_bed_temperature,
                           this->printer_config.printing_temperature);
    this->instructions += CALIBRATE_PRINTER;
    this->set_feedrate();

    auto const max_layers
        = amt_slices.value_or(this->slice_calculator->slices().size());
    for (auto layer_idx = 0; layer_idx < max_layers; layer_idx += 1)
    {
        // move nozzle to z value in preparation of next layer
        this->move_z(layer_idx);

        // fan off for 1st layer
        if (layer_idx == 0) { this->instructions += TURN_FAN_OFF + "\n"; }

        this->instructions += fmt::v7::format(";Layer {}\n", layer_idx);
        this->slice_into_gcode(layer_idx);

        // turn fan on after 1st layer
        if (layer_idx == 0) { this->instructions += TURN_FAN_ON + "\n"; }
    }

    this->instructions += PRINTING_CLEANUP_SNIPPET;

    return this->instructions;
}

void GcodeGenerator::print_xy(Point2D const& prev_point,
                              Point2D const& next_point)
{
    double const distance_to_cover
        = sqrt(pow(prev_point.x - next_point.x, 2.0)
               + pow(prev_point.y - next_point.y, 2.0));
    double const amount_to_extrude
        = distance_to_cover == 0
              ? 0
              : (this->printer_config.slice_height
                 * this->printer_config.nozzle_diameter * distance_to_cover)
                    / this->filament_area;

    this->total_extruded += amount_to_extrude;

    this->instructions += fmt::v7::format("G1 X{} Y{} E{}\n",
                                          next_point.x + this->print_offset.x,
                                          next_point.y + this->print_offset.y,
                                          this->total_extruded);
}

void GcodeGenerator::move_xy(Point2D const& point)
{
    // use relative to easily lift the nozzle & retract filament
    this->instructions += "G91\n";
    this->instructions += fmt::v7::format("G1 Z{} E{}\n",
                                          PREVENT_KNOCK_OVER_HEIGHT,
                                          -PREVENT_OOZING_EXTRUDER_VALUE);

    // use absolute to move to desired coordinate
    this->instructions += "G90\n";
    this->instructions += fmt::v7::format("G1 X{} Y{}\n",
                                          point.x + this->print_offset.x,
                                          point.y + this->print_offset.y);

    // and back relative to move nozzle down
    this->instructions += "G91\n";
    this->instructions += fmt::v7::format("G1 Z{} E{}\n",
                                          -PREVENT_KNOCK_OVER_HEIGHT,
                                          PREVENT_OOZING_EXTRUDER_VALUE);

    // back to absolute by default (in preparation for [x,y] movements after
    // this)
    this->instructions += "G90\n";
}

// TODO: improve path optimization
ClipperLib::Paths optimise_paths(ClipperLib::Paths const& paths)
{
    // assume 2 similar lines right next to each other (e.g. | |). If they both
    // start on top and end on bottom, the print head will have to travel a
    // horizontal and an extra vertical distance.
    //
    // However, now assume the 2nd line is reversed such that it starts on the
    // bottom and ends on top. The print head will only have to travel the short
    // horizontal distance.
    //
    // Input paths were generated such that they start clustered and end
    // clustered. This is why we reverse every 2nd path.
    //
    // The "close to each other" assumption is realised by the sorting.
    ClipperLib::Paths result;
    result.reserve(paths.size());
    std::copy(paths.begin(), paths.end(), std::back_inserter(result));

    result = sort_paths_head_tail(result);

    auto skip = false;
    for (auto& path : result)
    {
        skip = !skip;
        if (skip) { continue; }

        std::reverse(path.begin(), path.end());
    }
    return result;
}

void GcodeGenerator::paths_into_gcode(ClipperLib::Paths const& paths,
                                      bool const               paths_are_closed)
{
    auto const optimised_paths = optimise_paths(paths);

    for (auto const& path : optimised_paths)
    {
        std::optional<Point2D> prev_point = std::nullopt;

        auto const perimeter_in_points = path_to_points(path, paths_are_closed);
        for (auto const& point : perimeter_in_points)
        {
            if (!prev_point.has_value())
            {
                // speed up movement without extrusion
                this->set_feedrate(FREE_MOVEMENT_SPEED_FACTOR);
                this->move_xy(point);
                // reset speed
                this->set_feedrate();
            }
            else
            {
                this->print_xy(prev_point.value(), point);
            }
            prev_point = { point };
        }
    }
}

void GcodeGenerator::slice_into_gcode(uint const slice_idx)
{
    auto const& slice_opt = this->slice_calculator->get_slice(slice_idx);

    if (slice_opt.has_value())
    {
        auto const& slice = slice_opt.value();

        // following is the order in which Cura prints
        this->paths_into_gcode(slice.get_support().get_support_structure(),
                               false);

        for (auto const& shell : slice.get_shells())
        {
            this->paths_into_gcode(shell, true);
        }
        this->paths_into_gcode(slice.get_perimeters(), true);

        this->paths_into_gcode(slice.get_infill(), false);
        this->paths_into_gcode(slice.get_floor().get_filled_out_shell(), false);
        this->paths_into_gcode(slice.get_roof().get_filled_out_shell(), false);

        this->instructions += "\n";
    }
}