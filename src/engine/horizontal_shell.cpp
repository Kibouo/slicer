#include "include/engine/horizontal_shell.hpp"
#include "include/engine/infill_type.hpp"
#include "include/utils.hpp"

HorizontalShell::HorizontalShell(double const             nozzle_diameter,
                                 uint const               max_generations,
                                 ClipperLib::Paths const& contour)
    : max_generations(max_generations)
    , nozzle_diameter(nozzle_diameter)
    , rotate_lines(false)
{
    this->contour = { { contour, 0 } };
    this->fill_out_shell();
}

HorizontalShell::HorizontalShell(HorizontalShell const&   other,
                                 ClipperLib::Paths const& contour)
    : max_generations(other.max_generations)
    , nozzle_diameter(other.nozzle_diameter)
    , rotate_lines(!other.rotate_lines)
{
    this->contour = { { contour, 0 } };
    for (auto const& cont_and_generation : other.contour)
    {
        if (cont_and_generation.second + 1 <= this->max_generations)
        {
            this->contour.push_back(
                { cont_and_generation.first, cont_and_generation.second + 1 });
        }
    }

    this->fill_out_shell();
}

void HorizontalShell::fill_out_shell()
{
    ClipperLib::Clipper clipper;
    // only the innermost shell should be considered. Other shells already have
    // no space in-between them.
    clipper.AddPaths(this->get_contour(), ClipperLib::PolyType::ptClip, true);

    auto const infill_structure
        = generate_infill_structure(this->nozzle_diameter,
                                    0,
                                    clipper.GetBounds(),
                                    100,
                                    InfillType::Lines,
                                    this->rotate_lines ? 90 : 0);

    clipper.AddPaths(infill_structure, ClipperLib::PolyType::ptSubject, false);

    ClipperLib::PolyTree tree_solution;
    clipper.Execute(ClipperLib::ClipType::ctIntersection,
                    tree_solution,
                    ClipperLib::PolyFillType::pftEvenOdd,
                    ClipperLib::PolyFillType::pftEvenOdd);

    PolyTreeToPaths(tree_solution, this->filled_out_shell);
}

ClipperLib::Paths HorizontalShell::get_contour() const
{
    // contour parts need to be store separately to remember their generation,
    // but working with 1 whole contour is nicer (shell will visibly be printed
    // as a whole) and easier.
    ClipperLib::Clipper clipper;
    for (auto const& c : this->contour)
    {
        clipper.AddPaths(c.first, ClipperLib::PolyType::ptSubject, true);
    }

    ClipperLib::Paths solution;
    clipper.Execute(ClipperLib::ClipType::ctUnion,
                    solution,
                    ClipperLib::PolyFillType::pftEvenOdd,
                    ClipperLib::PolyFillType::pftEvenOdd);

    return solution;
}
