#include "include/engine/slice_plane.hpp"
#include <iostream>

SlicePlane::SlicePlane(glm::mat4 const model_matrix,
                       GLint const     model_matrix_handle,
                       GLint const     color_handle,
                       float           z_layer)
    : _model_matrix(model_matrix)
    , _model_matrix_handle(model_matrix_handle)
    , _color_handle(color_handle)
{
    Vertex p1{
        -PLANE_DIM,
        -PLANE_DIM,
        z_layer,
    };
    Vertex p2{ PLANE_DIM, -PLANE_DIM, z_layer };
    Vertex p3{ -PLANE_DIM, PLANE_DIM, z_layer };
    Vertex p4{ PLANE_DIM, PLANE_DIM, z_layer };
    Vertex p5{ -PLANE_DIM, PLANE_DIM, z_layer };
    Vertex p6{ PLANE_DIM, -PLANE_DIM, z_layer };
    this->_vertices = { p1, p2, p3, p4, p5, p6 };

    std::cout << "Size of vertices: " << this->_vertices.size() << std::endl;

    glGenVertexArrays(1, &this->_vao);
    glBindVertexArray(this->_vao);

    glGenBuffers(1, &this->_vertex_buffer);
}

void SlicePlane::update_z_layer(float z)
{
    if (z != _vertices[0].z) { std::cout << "New Z-layer: " << z << std::endl; }

    for (size_t idx = 0; idx < _vertices.size(); idx++)
    {
        _vertices[idx].z = z;
    }
}

void SlicePlane::draw() const
{
    glBindVertexArray(this->_vao);
    glBindBuffer(GL_ARRAY_BUFFER, this->_vertex_buffer);
    glBufferData(GL_ARRAY_BUFFER,
                 sizeof(Vertex) * this->_vertices.size(),
                 this->_vertices.data(),
                 GL_STATIC_DRAW);

    // Color and viewing matrix
    glUniform4fv(this->_color_handle, 1, &PLANE_COLOR.r);
    glUniformMatrix4fv(
        this->_model_matrix_handle, 1, GL_FALSE, &this->_model_matrix[0][0]);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);

    glDrawArrays(GL_TRIANGLES, 0, this->_vertices.size() * 3);
    glDisableVertexAttribArray(0);
}

SlicePlane::~SlicePlane()
{
    // glDeleteVertexArrays(1, &this->_vao);
    // glDeleteBuffers(1, &this->_vertex_buffer);
}