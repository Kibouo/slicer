#include "include/engine/shader_program.hpp"
#include "include/utils.hpp"

#ifdef __APPLE__
//#include <OpenGL/gl.h>
#define GL_SILENCE_DEPRECATION
#include <OpenGL/gl3.h>
#else
#include <GL/gl.h>
#endif

#include <iostream>
#include <vector>

void check_compile_status(std::filesystem::path const& shader_file,
                          GLuint const                 shader_handle)
{
    GLint success = GL_FALSE;

    glGetShaderiv(shader_handle, GL_COMPILE_STATUS, &success);
    if (success == GL_FALSE)
    {
        GLint log_size = 0;
        glGetShaderiv(shader_handle, GL_INFO_LOG_LENGTH, &log_size);

        std::vector<char> log(log_size + 1);
        glGetShaderInfoLog(shader_handle, log_size, NULL, &log[0]);
        std::cerr << "Shader compilation error in " << shader_file << "\n"
                  << &log[0] << std::endl;

        glDeleteShader(shader_handle);
        exit(-1);
    }
}

void check_link_status(GLuint const program_handle)
{
    GLint success = GL_FALSE;

    glGetProgramiv(program_handle, GL_LINK_STATUS, &success);
    if (success == GL_FALSE)
    {
        GLint log_size = 0;
        glGetProgramiv(program_handle, GL_INFO_LOG_LENGTH, &log_size);

        std::vector<char> log(log_size + 1);
        glGetProgramInfoLog(program_handle, log_size, NULL, &log[0]);
        std::cerr << "Program linking error: " << &log[0] << std::endl;

        glDeleteProgram(program_handle);
        exit(-1);
    }
}

GLuint compile_shader(std::filesystem::path const& shader_file,
                      GLenum const                 shader_type)
{
    auto        shader_handle = glCreateShader(shader_type);
    auto const  contents      = read_file(shader_file);
    char const* shader_code   = contents.c_str();

    glShaderSource(shader_handle, 1, &shader_code, NULL);
    glCompileShader(shader_handle);

    check_compile_status(shader_file, shader_handle);

    return shader_handle;
}

GLuint link_program(std::vector<GLuint>& shaders)
{
    auto program_handle = glCreateProgram();

    for (auto& shader : shaders) { glAttachShader(program_handle, shader); }
    glLinkProgram(program_handle);

    check_link_status(program_handle);

    return program_handle;
}

ShaderProgram::ShaderProgram(std::filesystem::path const& vertex_shader,
                             std::filesystem::path const& fragment_shader)
{
    std::vector<GLuint> shaders
        = { compile_shader(vertex_shader, GL_VERTEX_SHADER),
            compile_shader(fragment_shader, GL_FRAGMENT_SHADER) };

    this->program_handle = link_program(shaders);

    for (auto& shader : shaders)
    {
        glDetachShader(this->program_handle, shader);
        glDeleteShader(shader);
    }
}

ShaderProgram::~ShaderProgram() { glDeleteProgram(this->program_handle); }

void ShaderProgram::use() const { glUseProgram(this->program_handle); }

GLint ShaderProgram::uniform_location(std::string const& item) const
{
    return glGetUniformLocation(this->program_handle, item.c_str());
}