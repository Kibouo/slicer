#include "include/engine/support_layer.hpp"
#include "include/utils.hpp"

float const AMT_NOZZLES_SUPPORT_IS_FROM_PERIM = 2.5;

SupportLayer::SupportLayer(double const               nozzle_diameter,
                           ClipperLib::IntRect const& bounds,
                           ClipperLib::Paths const&   support_requiring_area)
    : nozzle_diameter(nozzle_diameter), bounds(bounds)
{
    // the dilation of the perimeter was needed to exclude self-supporting
    // areas. However, this also erodes actual support requiring areas as a
    // result. We want to union all support requiring areas to create the least
    // amount of supports. This is because it allows supports to be printed in 1
    // go.
    // But also, maybe more importantly, the actual support structure will be
    // eroded by a fell nozzle widths to ensure it doesn't stick to the
    // perimeter of the print. Tiny support requiring areas would erode as well
    // and could end up dissapearing because of this. We obviously don't want
    // this as the small area would be left floating.
    this->current_layer_support_requiring_area
        = offset(support_requiring_area, this->nozzle_diameter / 2.0);
    this->inherited_support_requiring_area = {};
    this->support_structure                = {};
}

SupportLayer::SupportLayer(SupportLayer const&      upper_layer_support,
                           ClipperLib::Paths const& model_perimeters,
                           ClipperLib::Paths const& support_requiring_area)
    : nozzle_diameter(upper_layer_support.nozzle_diameter)
    , bounds(upper_layer_support.bounds)
{
    // See other constructor for reasoning
    this->current_layer_support_requiring_area
        = offset(support_requiring_area, this->nozzle_diameter / 2.0);

    ClipperLib::Clipper clipper;
    clipper.AddPaths(upper_layer_support.current_layer_support_requiring_area,
                     ClipperLib::PolyType::ptSubject,
                     true);
    clipper.AddPaths(upper_layer_support.inherited_support_requiring_area,
                     ClipperLib::PolyType::ptClip,
                     true);

    clipper.Execute(ClipperLib::ClipType::ctUnion,
                    this->inherited_support_requiring_area,
                    ClipperLib::PolyFillType::pftEvenOdd,
                    ClipperLib::PolyFillType::pftEvenOdd);

    // only add supports where there will be on the layer below as well. This
    // matters in the case that the above layer has supports somewhere, but the
    // lower layer has a part of the model under that support. We don't want to
    // print supports over/through the model.
    /* Example: "table" with something sticking out. s = support
        -------
        |sssss|
        |SSsss|
        ---sss|
        |sssss|
    */
    // NOTE: the support resting on a part of the model will still stick to the
    // model (see capital S in example).
    // TODO: can something be done about the above note?
    clipper.Clear();
    clipper.AddPaths(this->inherited_support_requiring_area,
                     ClipperLib::PolyType::ptSubject,
                     true);
    clipper.AddPaths(model_perimeters, ClipperLib::PolyType::ptClip, true);

    clipper.Execute(ClipperLib::ClipType::ctDifference,
                    this->inherited_support_requiring_area,
                    ClipperLib::PolyFillType::pftEvenOdd,
                    ClipperLib::PolyFillType::pftEvenOdd);

    this->generate_support_structure();
}

void SupportLayer::generate_support_structure()
{
    // erode actual support area such that support structure is not printed
    // against the outer shell. Similar to how the highest layer of the support
    // is not printed.
    auto const eroded_support_area
        = offset(this->inherited_support_requiring_area,
                 -this->nozzle_diameter * AMT_NOZZLES_SUPPORT_IS_FROM_PERIM);

    ClipperLib::Clipper clipper;
    clipper.AddPaths(eroded_support_area, ClipperLib::PolyType::ptClip, true);

    auto const infill_structure = generate_infill_structure(
        this->nozzle_diameter, 0, this->bounds, 20, InfillType::Lines, -45);

    clipper.AddPaths(infill_structure, ClipperLib::PolyType::ptSubject, false);

    ClipperLib::PolyTree tree_solution;
    clipper.Execute(ClipperLib::ClipType::ctIntersection,
                    tree_solution,
                    ClipperLib::PolyFillType::pftEvenOdd,
                    ClipperLib::PolyFillType::pftEvenOdd);

    PolyTreeToPaths(tree_solution, this->support_structure);
}