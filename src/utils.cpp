#include "include/utils.hpp"
#include "include/engine/slice.hpp"
#include <algorithm>
#include <cmath>
#include <fstream>
#include <glm/glm.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/vec2.hpp>
#include <iostream>

std::string read_file(std::filesystem::path const& file_path)
{
    std::ifstream file(file_path);
    std::string   content((std::istreambuf_iterator<char>(file)),
                        std::istreambuf_iterator<char>());

    return content;
}

void write_file(std::filesystem::path const& file_path,
                std::string const&           content)
{
    std::ofstream file(file_path);
    std::copy(
        content.begin(), content.end(), std::ostreambuf_iterator<char>(file));
}

std::vector<std::string> split_string(std::string const& data,
                                      char const         delimiter)
{
    std::vector<std::string> parts{};
    std::string              tmp = "";

    for (auto const& c : data)
    {
        if (c == delimiter)
        {
            parts.push_back(tmp);
            tmp = "";
        }
        else
        {
            tmp += c;
        }
    }

    parts.push_back(tmp);
    return parts;
}

std::vector<Point2D> path_to_points(ClipperLib::Path const& path,
                                    bool const              path_is_closed)
{
    std::vector<Point2D> result{};

    for (const auto& point : path)
    {
        result.push_back({ float(double(point.X) / double(INT_POINT_PREC)),
                           float(double(point.Y) / double(INT_POINT_PREC)) });
    }

    if (path_is_closed)
    {
        // add first point to complete the contour
        result.push_back(
            { float(double(path.front().X) / double(INT_POINT_PREC)),
              float(double(path.front().Y) / double(INT_POINT_PREC)) });
    }

    return result;
}

void print_vec_of_point2d(std::vector<Point2D> const& points)
{
    std::cout << "std::vector<Point2D>" << std::endl;
    for (size_t i = 0; i < points.size(); i++)
    {
        std::cout << "X => " << points[i].x << " Y => " << points[i].y
                  << std::endl;
    }
}

bool Point2D::equals(Point2D const& rhs, float const epsilon_factor) const
{
    auto const epsilon = 0.0001 * epsilon_factor;
    return std::fabs(this->x - rhs.x) < epsilon
           && std::fabs(this->y - rhs.y) < epsilon;
}

Point2D operator+(Point2D const& lhs, Point2D const& rhs)
{
    return { lhs.x + rhs.x, lhs.y + rhs.y };
}

float distance(ClipperLib::IntPoint const& a, ClipperLib::IntPoint const& b)
{
    return glm::distance(glm::vec2{ a.X, a.Y }, glm::vec2{ b.X, b.Y });
}

// Sort paths such that the distance between 2 consecutive paths (1st's end and
// 2nd's start) is minimal.
ClipperLib::Paths& sort_paths_head_tail(ClipperLib::Paths& paths)
{
    std::sort(paths.begin(),
              paths.end(),
              [](ClipperLib::Path& a, ClipperLib::Path& b) {
                  return distance(a.back(), b.front())
                         < distance(a.front(), b.back());
              });

    return paths;
}

// TODO: improve offset operation's efficiency (lot of time is spent on this)
ClipperLib::Paths offset(ClipperLib::Paths const& paths, double const delta)
{
    ClipperLib::ClipperOffset clipper_offset;
    ClipperLib::Paths         solution;

    clipper_offset.AddPaths(
        paths, ClipperLib::jtRound, ClipperLib::etClosedPolygon);
    clipper_offset.Execute(solution, delta * INT_POINT_PREC);

    return solution;
}

ClipperLib::Paths generate_infill_structure(
    double const               nozzle_diameter,
    uint const                 layer_idx,
    ClipperLib::IntRect const& bounds,
    uint8_t const              infill_density_percentage,
    InfillType const           type,
    float const                default_rotation)
{
    ClipperLib::Paths infill{};
    if (infill_density_percentage == 0) { return infill; }

    auto const height = abs(bounds.top - bounds.bottom);
    auto const width  = abs(bounds.right - bounds.left);

    // Clipper sometimes mixes up the values and where it assigns them to.
    // Probably some bug in the code that generates input, but for now it
    // worksTM.
    auto const left   = std::min(bounds.left, bounds.right);
    auto const bottom = std::min(bounds.bottom, bounds.top);

    std::vector<float> rotations{};
    // With a 100% infill density, each line will be squished next to
    // another. Thus, there will be no more space left in-between the lines to
    // draw another line at an angle.
    if (infill_density_percentage == 100) { rotations = { 45.0 }; }
    else
    {
        switch (type)
        {
            case InfillType::Rectangular: rotations = { 45.0, -45.0 }; break;
            case InfillType::Cubic:
                // no break as cubic is simply triangles + offset (added later)
            case InfillType::Triangles:
                rotations = { 20.0, 140.0, 260.0 };
                break;
            case InfillType::Lines: rotations = { 45.0 }; break;
            default: break;
        }
    }

    // multiply `line_dist` with multiple of lines that will be drawn. This
    // compensates for high density infills
    int line_dist
        = INT_POINT_PREC
          * (nozzle_diameter / (float(infill_density_percentage) / 100.0));
    line_dist *= rotations.size();

    int offset = 0;
    if (type == InfillType::Cubic)
    {
        // add half (not whole to ensure layers connect, similar to
        // overhangs) nozzle thickness as offset.
        offset = INT_POINT_PREC * (float(layer_idx) * nozzle_diameter / 2.0);
        // perform modulo to ensure highest layers doesn't offset the
        // starting line so much that it starts halfway the slice
        offset %= line_dist;
    }

    // 2 notable things happen here:
    // - make the grid square with 2x max(width, height) as side length. This
    // ensures that the rotated lines cover the whole model.
    // - build grid around 0 such that rotation with glm is easy
    auto const extreme = std::max(width, height);
    for (int x = -extreme + offset; x <= extreme + offset; x += line_dist)
    {
        for (float const rotation : rotations)
        {
            auto const head
                = glm::rotate(glm::vec2(x, extreme),
                              glm::radians(rotation + default_rotation));
            auto const tail
                = glm::rotate(glm::vec2(x, -extreme),
                              glm::radians(rotation + default_rotation));
            infill.push_back(
                { ClipperLib::IntPoint{
                      (ClipperLib::cInt)(head.x + extreme / 2 + left),
                      (ClipperLib::cInt)(head.y + extreme / 2 + bottom) },
                  ClipperLib::IntPoint{
                      (ClipperLib::cInt)(tail.x + extreme / 2 + left),
                      (ClipperLib::cInt)(tail.y + extreme / 2 + bottom) } });
        }
    }

    return infill;
}