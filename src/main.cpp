
#ifdef __APPLE__
#define GL_SILENCE_DEPRECATION
#endif

#include "include/engine/engine.hpp"
#include "include/gui/window.hpp"
#include <gtk/gtk.h>
#include <iostream>
#include <optional>
#include <vector>

int main(int argc, char* argv[])
{
    if (!gtk_init_check(&argc, &argv))
    {
        std::cerr << "GTK failed to initialize.";
        exit(-1);
    }

    // could use an ECS and pass that to `build_gui`, but that'd be "too much"
    // for this project
    // GTK wants to be the one to initialise the OpenGL context. We just pretend
    // that's fine and let them fill out this pointer. It's also wrapped in an
    // optional, as GTK simply ignores nullptr's passed to it...
    std::optional<Engine*> engine = std::nullopt;

    auto const windows = build_gui(engine);
    for (auto const window : windows)
    {
        gtk_widget_show_all(GTK_WIDGET(window));
    }
    gtk_main();

    delete engine.value();
    return 0;
}