#pragma once

#ifdef __APPLE__
#define GL_SILENCE_DEPRECATION
#include <OpenGL/gl3.h>
#else
#include <GL/gl.h>
#endif

#include "include/engine/model.hpp"
#include <array>
#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>
#include <vector>

auto const  PLANE_COLOR = glm::vec4{ 0.0, 1.0, 0.0, 1.0 };    // green
float const PLANE_DIM   = 90.0;

class SlicePlane
{
    public:
    SlicePlane(glm::mat4 const model_matrix,
               GLint const     model_matrix_handle,
               GLint const     color_handle,
               float           z_layer);
    ~SlicePlane();
    void draw() const;

    void update_z_layer(float z);

    private:
    GLuint              _vao;
    GLuint              _vertex_buffer;
    glm::mat4 const     _model_matrix;
    std::vector<Vertex> _vertices;
    GLint const         _color_handle;
    GLint const         _model_matrix_handle;
};