#pragma once

#include "include/engine/printer_config.hpp"
#include "include/engine/slice.hpp"
#include "include/engine/slice_calculator.hpp"
#include <memory>
#include <optional>
#include <string>

class GcodeGenerator
{
    public:
    GcodeGenerator(std::shared_ptr<SliceCalculator> const slice_calculator,
                   PrinterConfig const&                   printer_config,
                   glm::mat4 const&                       model_matrix);
    std::string generate_gcode(std::optional<uint> const amt_slices
                               = std::nullopt);

    private:
    void set_feedrate(float const speed_up_factor = 1.0);
    void move_xy(Point2D const& point);
    void print_xy(Point2D const& prev_point, Point2D const& next_point);
    void move_z(uint const slice_idx);

    void paths_into_gcode(ClipperLib::Paths const& paths,
                          bool const               paths_are_closed);
    void slice_into_gcode(uint const slice_idx);

    std::shared_ptr<SliceCalculator> const slice_calculator;
    PrinterConfig const&                   printer_config;
    double const                           filament_area;
    double                                 total_extruded = 0;
    std::string                            instructions   = "";

    // pre-calculated offset to points. Uses model matrix and print bed
    // dimensions to ensure that the center of the print will be at the center
    // of the print bed.
    Point2D print_offset;
};

std::string const PRE_HEAT_TEMPLATE
    = "M140 S{0}                       ;set bed temperature\n"
      "M190 S{0}                       ;wait for bed temperature\n"
      "M104 S{1}                       ;set nozzle temperature\n"
      "M109 S{1}                       ;wait for nozzle temperature\n\n";
std::string const CALIBRATE_PRINTER
    = "G21                             ;use metric (mm/min)\n"
      "M82                             ;absolute extrusion mode\n"
      "G92 E0                          ;Reset Extruder\n"
      "G28                             ;Home all axes\n"
      "G1 Z2.0 F3000                   ;Move Z Axis up to prevent scratching of Heat Bed\n"
      "G1 X0.1 Y20 Z0.3 F5000.0        ;Move to start position\n"
      "G1 X0.1 Y200.0 Z0.3 F1500.0 E15 ;Draw the first line\n"
      "G1 X0.4 Y200.0 Z0.3 F5000.0     ;Move to side a little\n"
      "G1 X0.4 Y20 Z0.3 F1500.0 E30    ;Draw the second line\n"
      "G1 X80 F4000                    ;Quickly wipe away from the filament line\n"
      "G1 Z2.0 F3000                   ;Move Z Axis up to prevent scratching of Heat Bed\n"
      "G92 E0                          ;Reset Extruder\n"
      "G90                             ;Use absolute coordinates\n\n";
std::string const TURN_FAN_OFF
    = "M107                            ;fan off for first layer\n";
std::string const TURN_FAN_ON
    = "M106                            ;turn fan on\n";
std::string const PRINTING_CLEANUP_SNIPPET
    = "M140 S0                         ;set bed temperature\n"
      "M107                            ;fan off\n"
      "M220 S100                       ;Reset Speed factor override percentage to default (100%)\n"
      "M221 S100                       ;Reset Extrude factor override percentage to default (100%)\n"
      "G1 X0.1 Y20 F5000.0             ;Move to start position to prevent oozing on print\n"
      "G91                             ;Set coordinates to relative\n"
      "G1 F1800 E-3                    ;Retract filament 3 mm to prevent oozing\n"
      "G1 F3000 Z20                    ;Move Z Axis up 20 mm to allow filament ooze freely\n"
      "G90                             ;Set coordinates to absolute\n"
      "G1 X0 Y235 F1000                ;Move Heat Bed to the front for easy print removal\n"
      "M107                            ;fan off\n"
      "M84                             ;Disable stepper motors\n"
      "M82                             ;absolute extrusion mode\n"
      "M104 S0                         ;set extruder temperature\n\n";
// extra height to raise nozzle. Used to prevent scratching of bed and prevent
// knocking the model over.
double const PREVENT_KNOCK_OVER_HEIGHT     = 2.0;
double const PREVENT_OOZING_EXTRUDER_VALUE = 10.0;
// factor to speed up movement during movements without extrusion
float const FREE_MOVEMENT_SPEED_FACTOR = 5.0;