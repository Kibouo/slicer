#pragma once

#ifdef __APPLE__
#define GL_SILENCE_DEPRECATION
#include <OpenGL/gl3.h>
#else
#include <GL/gl.h>
#endif

#include <array>
#include <filesystem>
#include <glm/mat4x4.hpp>
#include <vector>

auto const MODEL_COLOR = glm::vec4{1.0, 0.0, 0.0, 1.0};

typedef struct
{
    float x;
    float y;
    float z;
} Vertex;

typedef struct
{
    float r;
    float g;
    float b;
    float a;
} Color;

class SliceCalculator;

class Model
{
    public:
    static std::unique_ptr<Model> from_stl(std::filesystem::path const& file,
                                           GLint const model_matrix_handle,
                                           GLint const color_handle);
    void                          draw() const;
    Model() = default;
    Model(glm::mat4 const              model_matrix,
          GLint const                  model_matrix_handle,
          GLint const                  color_handle,
          std::vector<Vertex> const    vertices,
          Color const                  color,
          std::filesystem::path const& source_file);
    ~Model();
    void slicing_plane_up();
    void slicing_plane_down();

    std::filesystem::path const& get_source_file() const
    {
        return this->_source_file;
    }
    const std::vector<Vertex> get_vertices() const { return vertices; }
    glm::mat4 const& get_model_matrix() const { return this->model_matrix; }

    uint slicing_plane() const { return this->_slicing_plane; }

    private:
    std::vector<Vertex> const vertices;

    GLuint vao;
    GLuint vertex_buffer;

    glm::mat4 const model_matrix;
    Color const     color;
    uint            _slicing_plane;

    GLint const model_matrix_handle;
    GLint const color_handle;

    std::filesystem::path const _source_file;
};