#pragma once

#ifdef __APPLE__
//#include <OpenGL/gl.h>
typedef unsigned int uint;
#define GL_SILENCE_DEPRECATION
#include <OpenGL/gl3.h>
#else
#include <GL/gl.h>
#endif

#include <glm/mat4x4.hpp>

float const ZNEAR = 0.1;
float const ZFAR  = 255.0;

class Camera
{
    public:
    Camera(GLint const view_matrix_handle,
           GLint const projection_matrix_handle);
    void         use() const;
    virtual void aspect_ratio(uint const width, uint const height) = 0;

    protected:
    glm::mat4   view_matrix;
    glm::mat4   projection_matrix;
    GLint const view_matrix_handle;
    GLint const projection_matrix_handle;
};