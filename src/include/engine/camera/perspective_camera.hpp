#pragma once

#ifdef __APPLE__
//#include <OpenGL/gl.h>
typedef unsigned int uint;
#define GL_SILENCE_DEPRECATION
#include <OpenGL/gl3.h>
#else
#include <GL/gl.h>
#endif
#include <glm/mat4x4.hpp>

#include "include/engine/camera/camera.hpp"

class PerspectiveCamera : public Camera
{
    public:
    PerspectiveCamera(GLint const view_matrix_handle,
                      GLint const projection_matrix_handle);
    virtual void aspect_ratio(uint const width, uint const height);
};