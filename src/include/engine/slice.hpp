#ifndef SLICE_H
#define SLICE_H

#include <vector>

#ifdef __APPLE__
#define GL_SILENCE_DEPRECATION
#include <OpenGL/gl3.h>
#else
#include <GL/gl.h>
#endif

#include "clipper.hpp"
#include "include/engine/horizontal_shell.hpp"
#include "include/engine/infill_type.hpp"
#include "include/engine/support_layer.hpp"
#include "include/utils.hpp"
#include <array>
#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>

const int INT_POINT_PREC = 10000;

class Slice
{
    public:
    Slice(uint const                                layer_idx,
          std::vector<std::array<Point2D, 2>> const slicing_edges,
          double const                              nozzle_diameter,
          ClipperLib::IntRect const                 bounds,
          glm::mat4 const                           model_matrix,
          GLint const                               color_handle,
          GLint const                               model_matrix_handle);
    ~Slice();

    ClipperLib::Paths const& get_perimeters() const { return this->perimeters; }
    std::vector<ClipperLib::Paths> const& get_shells() const
    {
        return this->shells;
    }
    ClipperLib::Paths const& get_infill() const { return this->infill; }
    HorizontalShell const&   get_floor() const { return this->floor; }
    HorizontalShell const&   get_roof() const { return this->roof; }
    SupportLayer const&      get_support() const { return this->support; }

    std::string to_string() const;
    void        draw() const;

    void erode_slice();
    void create_shells(uint const amount);
    void generate_infill(uint8_t const    infill_density_percentage,
                         InfillType const type);
    void build_floor(std::optional<Slice> const& prev_slice,
                     uint const                  amount_shells);
    void build_roof(std::optional<Slice> const& next_slice,
                    uint const                  amount_shells);
    void generate_supports(std::optional<Slice> const& next_slice);

    private:
    void                     draw_paths(ClipperLib::Paths const& paths,
                                        glm::vec3 const          color,
                                        bool const               paths_are_closed) const;
    ClipperLib::Paths const& get_innermost_shell() const;
    ClipperLib::Paths        build_horizontal_shell_contour(
               std::optional<Slice> const& other_slice,
               uint const                  amount_shells) const;

    uint const                layer_idx;
    double const              nozzle_diameter;
    ClipperLib::IntRect const bounds;

    ClipperLib::Paths perimeters;
    // should be ordered from inside to out
    std::vector<ClipperLib::Paths> shells;
    HorizontalShell                floor;
    HorizontalShell                roof;
    SupportLayer                   support;
    ClipperLib::Paths              infill;

    GLuint      vao;
    GLuint      vertex_buffer;
    glm::mat4   model_matrix;
    GLint const color_handle;
    GLint const model_matrix_handle;
};

#endif