#pragma once

#include "clipper.hpp"
#include <vector>

// Used as either a roof or floor.
// Handles the inheriting and generating of roof/floor infill structures. The
// actual area to be considered is expected as input.
class HorizontalShell
{
    public:
    HorizontalShell() = default;
    HorizontalShell(double const             nozzle_diameter,
                    uint const               max_generations,
                    ClipperLib::Paths const& contour);
    HorizontalShell(HorizontalShell const&   other,
                    ClipperLib::Paths const& contour);

    ClipperLib::Paths        get_contour() const;
    ClipperLib::Paths const& get_filled_out_shell() const
    {
        return this->filled_out_shell;
    };

    private:
    void fill_out_shell();

    // only the contours of the shell along with their "generation".
    // Generation indicates how many layers away the contour originates from.
    // This is used, along with the `max_generations` to limit the infite
    // stacking of HorizontalShells (as they are supposed to be inherited for
    // printing stability).
    std::vector<std::pair<ClipperLib::Paths, uint>> contour;
    ClipperLib::Paths                               filled_out_shell;

    uint   max_generations;
    double nozzle_diameter;
    bool   rotate_lines;
};