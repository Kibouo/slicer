#pragma once

#include "include/engine/camera/orthographic_camera.hpp"
#include "include/engine/camera/perspective_camera.hpp"
#include "include/engine/model.hpp"
#include "include/engine/printer_config.hpp"
#include "include/engine/shader_program.hpp"
#include "include/engine/slice_calculator.hpp"
#include "include/engine/slice_plane.hpp"
#include <memory>

class Engine
{
    public:
    Engine();
    void draw_model() const;
    void draw_slice() const;
    void load_model(std::filesystem::path const& stl_file);
    void update_printer_config(PrinterConfig const& printer_config);
    void slicing_plane_up();
    void slicing_plane_down();
    PrinterConfig const& printer_config() const
    {
        return this->_printer_config;
    }
    std::shared_ptr<SliceCalculator> slice_calculator() const
    {
        return this->_slice_calculator;
    };

    std::unique_ptr<PerspectiveCamera>  model_camera;
    std::unique_ptr<OrthographicCamera> slice_camera;
    std::unique_ptr<Model>              model;
    std::unique_ptr<SlicePlane>         slice_plane;

    private:
    PrinterConfig                    _printer_config;
    std::unique_ptr<ShaderProgram>   model_program;
    std::unique_ptr<ShaderProgram>   slice_program;
    std::shared_ptr<SliceCalculator> _slice_calculator;
};