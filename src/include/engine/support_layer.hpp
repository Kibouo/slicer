#pragma once

#include "clipper.hpp"

// Handles the inheriting and generating of support structures. The actual area
// which needs supporting is expected as input.
class SupportLayer
{
    public:
    SupportLayer() = default;
    SupportLayer(double const               nozzle_diameter,
                 ClipperLib::IntRect const& bounds,
                 ClipperLib::Paths const&   support_requiring_area);
    SupportLayer(SupportLayer const&      upper_layer_support,
                 ClipperLib::Paths const& model_perimeters,
                 ClipperLib::Paths const& support_requiring_area);
    ClipperLib::Paths const& get_support_structure() const
    {
        return this->support_structure;
    }

    private:
    void generate_support_structure();

    // will not be considered for printing as 1 layer of support is to be left
    // out for easier removal of supports
    ClipperLib::Paths current_layer_support_requiring_area;
    // inherited support from higher up layers. Will be considered when
    // generating structure
    ClipperLib::Paths inherited_support_requiring_area;

    // the actual support structure to be printed
    ClipperLib::Paths support_structure;

    double              nozzle_diameter;
    ClipperLib::IntRect bounds;
};