#pragma once

#include "include/engine/infill_type.hpp"
#include "include/engine/shader_program.hpp"
#include "include/engine/slice.hpp"
#include <memory>
#include <optional>
#include <vector>

class Model;

class SliceCalculator
{
    public:
    SliceCalculator(double const     nozzle_diameter,
                    bool const       use_supports,
                    uint const       infill_density,
                    InfillType const infill_type,
                    double const     slice_height,
                    uint const       additional_shells,
                    GLint const      color_handle,
                    GLint const      model_matrix_handle);
    void calculateSlices(std::unique_ptr<Model> const& model);

    std::optional<Slice> const get_slice(uint const slice_idx) const
    {
        if (slice_idx < this->_slices.size())
        {
            return { this->_slices.at(slice_idx) };
        }
        else
        {
            return std::nullopt;
        }
    }

    std::vector<Slice> const& slices() const { return this->_slices; }

    float get_slice_height(uint const slice_idx) const
    {
        return slice_idx * slice_height;
    }

    private:
    double const       nozzle_diameter;
    bool const         use_supports;
    uint const         infill_density;
    InfillType const   infill_type;
    double const       slice_height;
    uint const         additional_shells;
    std::vector<Slice> _slices;    // will contain all slices

    GLint const color_handle;
    GLint const model_matrix_handle;
};
