#pragma once

#include "include/engine/infill_type.hpp"

typedef unsigned int uint;

struct PrinterConfig
{
    double nozzle_diameter   = 0.4;     // unit: mm
    double filament_diameter = 1.75;    // unit: mm
    uint   width_x           = 220;     // unit: mm
    uint   depth_y           = 220;     // unit: mm
    uint   height_z          = 250;     // unit: mm

    uint print_bed_temperature = 60;     // unit: degree Celsius
    uint printing_temperature  = 200;    // unit: degree Celsius
    uint printing_speed        = 20;     // unit: mm/s

    bool       use_supports      = false;
    uint       infill_density    = 20;    // unit: %
    InfillType infill_type       = InfillType::Rectangular;
    double     slice_height      = 0.2;    // unit: mm
    uint       additional_shells = 1;
};