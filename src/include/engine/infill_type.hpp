#pragma once

#include <string>
#include <vector>

// TODO: add zigzag infill
enum InfillType
{
    Rectangular,
    Triangles,
    Lines,
    Cubic
};

std::string              infill_type_into_string(InfillType const type);
std::vector<std::string> infill_enum_into_strings();