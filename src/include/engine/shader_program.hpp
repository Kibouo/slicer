#pragma once

#ifdef __APPLE__
//#include <OpenGL/gl.h>
#define GL_SILENCE_DEPRECATION
#include <OpenGL/gl3.h>
#else
#include <GL/gl.h>
#endif
#include <filesystem>

class ShaderProgram
{
    public:
    ShaderProgram() = default;
    ShaderProgram(std::filesystem::path const& vertex_shader,
                  std::filesystem::path const& fragment_shader);
    ~ShaderProgram();
    void  use() const;
    GLint uniform_location(std::string const& item) const;

    private:
    GLuint program_handle;
};