#pragma once

#include "include/engine/engine.hpp"
#include <gtk/gtk.h>
#include <optional>
#include <string>
#include <vector>

uint const        DEFAULT_WIDTH      = 1024;
uint const        DEFAULT_HEIGHT     = 768;
std::string const MAIN_WINDOW_TITLE  = "Slicer";
std::string const SLICE_WINDOW_TITLE = "Slice Plane";

std::vector<GtkWindow*> build_gui(std::optional<Engine*>& engine);
