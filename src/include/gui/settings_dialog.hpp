#pragma once

#include "include/engine/printer_config.hpp"
#include <gtk/gtk.h>
#include <optional>
#include <string>
#include <utility>

class SettingsDialog
{
    public:
    SettingsDialog(GtkWindow* window, PrinterConfig const& current_config);
    ~SettingsDialog();
    int run() const;
    // returns either a config or an error string
    std::pair<std::optional<PrinterConfig>, std::optional<std::string>>
    try_into_printer_config() const;

    private:
    GtkDialog* dialog;

    GtkSpinButton* nozzle_diameter_spinbox;
    GtkSpinButton* filament_diameter_spinbox;
    GtkSpinButton* width_x_spinbox;
    GtkSpinButton* depth_y_spinbox;
    GtkSpinButton* height_z_spinbox;

    GtkSpinButton* print_bed_temperature_spinbox;
    GtkSpinButton* printing_temperature_spinbox;
    GtkSpinButton* printing_speed_spinbox;

    GtkCheckButton* use_supports_checkbox;
    GtkSpinButton*  infill_density_spinbox;
    GtkComboBox*    infill_type_dropdown;
    GtkSpinButton*  slice_height_spinbox;
    GtkSpinButton*  additional_shells_spinbox;
};