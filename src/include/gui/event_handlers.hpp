#pragma once

#include "include/engine/engine.hpp"
#include <gtk/gtk.h>
#include <optional>
#include <string>

// GObject's have a KV store. To pass more than 1 param to events, this store is
// used. These are the keys for it.
std::string const WINDOW_KEY = "window";
std::string const ENGINE_KEY = "engine";

gboolean on_realize(GtkGLArea* glarea, std::optional<Engine*>* engine);
gboolean on_main_render(GtkGLArea*              glarea,
                        GdkGLContext*           glcontext,
                        std::optional<Engine*>* engine);
gboolean on_slice_render(GtkGLArea*              glarea,
                         GdkGLContext*           glcontext,
                         std::optional<Engine*>* engine);
void     on_main_resize(GtkGLArea*              glarea,
                        gint                    width,
                        gint                    height,
                        std::optional<Engine*>* engine);
void     on_slice_resize(GtkGLArea*              glarea,
                         gint                    width,
                         gint                    height,
                         std::optional<Engine*>* engine);
void     on_load_stl(GtkCheckMenuItem* menu_item);
void     on_edit_settings(GtkCheckMenuItem* menu_item);
gboolean on_key_press(GtkWindow*              window,
                      GdkEventKey*            event,
                      std::optional<Engine*>* engine);