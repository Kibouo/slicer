#pragma once

#include "clipper.hpp"
#include "include/engine/infill_type.hpp"
#include <filesystem>
#include <string>
#include <vector>

struct Point2D
{
    bool equals(Point2D const& rhs, float const epsilon_factor = 1.0) const;
    friend Point2D operator+(Point2D const& lhs, Point2D const& rhs);
    float          x;
    float          y;
};

std::string              read_file(std::filesystem::path const& file_path);
void                     write_file(std::filesystem::path const& file_path,
                                    std::string const&           content);
std::vector<std::string> split_string(std::string const& data,
                                      char const         delimiter);
std::vector<Point2D>     path_to_points(ClipperLib::Path const& path,
                                        bool const              path_is_closed);
void               print_vec_of_point2d(std::vector<Point2D> const& points);
ClipperLib::Paths& sort_paths_head_tail(ClipperLib::Paths& paths);
float distance(ClipperLib::IntPoint const& a, ClipperLib::IntPoint const& b);
ClipperLib::Paths offset(ClipperLib::Paths const& paths, double const delta);
ClipperLib::Paths generate_infill_structure(
    double const               nozzle_diameter,
    uint const                 layer_idx,
    ClipperLib::IntRect const& bounds,
    uint8_t const              infill_density_percentage,
    InfillType const           type,
    float const                default_rotation = 0);